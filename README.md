
The repository contains:

* A version of the Palcom browser with ComPOS integrated. To run `thebrowserthing-compos.jar`
* The source code for ComPOS without dependencies. Plan to publish a version that is possible to build.

[Alfred �kesson](http://cs.lth.se/alfred-akesson)

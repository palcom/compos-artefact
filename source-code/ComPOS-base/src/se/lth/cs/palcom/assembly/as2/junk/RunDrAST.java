//package se.lth.cs.palcom.assembly.as2.junk;
//
//import java.io.IOException;
//import java.io.RandomAccessFile;
//import java.nio.file.ReadOnlyFileSystemException;
//
//import org.jastadd.tree.edit.xml.JastAddXMLParser;
//
//import drast.views.gui.DrASTGUI;
//import internal.org.xmlpull.v1.XmlPullParserException;
//import se.lth.cs.palcom.assembly.as2.gen.As2Unit;
//
//public class RunDrAST {
//
//	public static void main(String[] args) throws XmlPullParserException, IOException, Exception {
//		As2Unit as2Assembly = (As2Unit) JastAddXMLParser.parse(new String(readFile("/Users/alfred/PalcomFilesystem/devices/C-5bfc408f-bff2-4d18-b7d4-7940cfb88c72/assemblies/testStfuu.as2")), "se.lth.cs.palcom.assembly.as2.gen");	
//		DrASTGUI debugger = new DrASTGUI(as2Assembly);// Where rootNode is the Object of your AST:s root.
//		debugger.run();
//	}
//	
//	public static byte[] readFile(String path) throws Exception{
//		RandomAccessFile f = new RandomAccessFile(path, "r");
//		byte[] data = new byte[(int)f.length()];
//		f.read(data);
//		return data;
//	}
//
//}

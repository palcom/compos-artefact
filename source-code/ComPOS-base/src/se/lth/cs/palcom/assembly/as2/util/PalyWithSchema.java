package se.lth.cs.palcom.assembly.as2.util;

import naming.Version;
import se.lth.cs.palcom.pon.schema.PONSchema;
import se.lth.cs.palcom.pon.schema.PrimitiveType.ObjectType;
import se.lth.cs.palcom.pon.schema.Schema;

public class PalyWithSchema {

	public static void main(String[] args) {
		Schema schema = PONSchema.getSchema(Version.class);
		schema.checkConsistency();
		schema.getDefinition();
		ObjectType i = (ObjectType) schema.getDefinition();
		i.getKeySet();
	}

}

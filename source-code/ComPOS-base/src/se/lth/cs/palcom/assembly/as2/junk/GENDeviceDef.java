package se.lth.cs.palcom.assembly.as2.junk;

import naming.Version;
import naming.VersionPart;
import naming.device.DeviceID;
import naming.device.DeviceInstanceID;
import naming.device.DeviceInstanceReference;
import naming.device.DeviceTypeID;
import naming.device.DeviceTypeVersionReference;
import se.lth.cs.palcom.pon.PON;

public class GENDeviceDef {

	public static void main(String args[]) {

		DeviceInstanceReference dir = new DeviceInstanceReference(
				new DeviceTypeVersionReference(new DeviceTypeID("none", new DeviceID("X:NONE"), "node"),
				new Version("none", new VersionPart(new DeviceID("X:NONE"), "none"))),
				new DeviceInstanceID("none", new DeviceID("X:NONE")));
		
	System.out.println(PON.toPONString(dir));

	}

}

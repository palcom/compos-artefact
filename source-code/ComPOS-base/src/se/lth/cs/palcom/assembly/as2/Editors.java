//package se.lth.cs.palcom.assembly.as2;
//
//import java.lang.reflect.Constructor;
//import java.lang.reflect.InvocationTargetException;
//import java.lang.reflect.Method;
//
//
//import se.lth.cs.palcom.assembly.as2.exception.As2Exception;
//import se.lth.cs.palcom.assembly.as2.gen.ASTNode;
//import se.lth.cs.palcom.assembly.as2.gen.List;
//import se.lth.cs.palcom.assembly.as2.gen.Opt;
//
//
//public class Editors {
//
//
//	public static ASTNode addToList(final List<? extends ASTNode> list, Class<?> toCreate,
//			SubclassExtractor se) {
//		ASTNode node;
//		try {
//			node = (ASTNode) toCreate.newInstance();
//			se.inferAll(node);
//			list.setChild(node, list.getNumChildNoTransform());
//		} catch (InstantiationException | IllegalAccessException e) {
//			throw new As2Exception("Error in add to List:" + list, e);
//		}
//		return node;
//	}
//
//	public static void editChild(ASTNode node, String name, Class<?> itm, SubclassExtractor se) {
//		try {
//
//			ASTNode o = (ASTNode) itm.newInstance();
//			se.inferAll(o);
//			Method get = node.getClass().getMethod("get" + name);
//			Method set = node.getClass().getMethod("set" + name, get.getReturnType());
//			ASTNode res = (ASTNode) get.invoke(node);
//			set.invoke(node, o);
//		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
//				| IllegalArgumentException | InvocationTargetException e) {
//			throw new As2Exception("Error in edit child:" + node + " has name " + name, e);
//		}
//	}
//
//	public static void setValue(String token, String val, ASTNode node) {
//		try {
//			Method mot = node.getClass().getMethod("set" + token, String.class);
//			mot.invoke(node, val);
//		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
//				| InvocationTargetException e) {
//			e.printStackTrace();
//			throw new As2Exception("Can not find method: " + "set" + token + " on " + node, e);
//		}
//	}
//
//	public static void removeOpt(ASTNode node, String name) {
//		try {
//			Method get2 = node.getClass().getMethod("get" + name);
//			Method get = node.getClass().getMethod("get" + name + "Opt");
//			Method set = node.getClass().getMethod("set" + name + "Opt", get.getReturnType());
//			Class<?> tmp0 = get.getReturnType();
//			ASTNode n = (ASTNode) get2.invoke(node);
//			set.invoke(node, tmp0.newInstance());
//		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
//				| InvocationTargetException | InstantiationException e) {
//			throw new As2Exception("Error in remove Opt: " + node + " has name " + name, e);
//		}
//	}
//
//	public static void addOpt(ASTNode node, String name, Class<?> toCreate, SubclassExtractor se) {
//		try {
//			Method get = node.getClass().getMethod("get" + name + "Opt");
//			Method set = node.getClass().getMethod("set" + name + "Opt", get.getReturnType());
//			Class<?> tmp0 = get.getReturnType();
//			Constructor<?>[] tmp = tmp0.getConstructors();
//			Constructor<?> tmp1 = null;
//			for (Constructor<?> t : tmp) {
//				if (t.getParameterTypes().length == 1) {
//					tmp1 = t;
//				}
//			}
//			ASTNode arg1 = (ASTNode) toCreate.newInstance();
//			se.inferAll(arg1);
//
//			Object tmp2 = tmp1.newInstance(arg1);
//			set.invoke(node, tmp2);
//		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
//				| InvocationTargetException | InstantiationException e) {
//			throw new As2Exception("Error in adding opt on: " + node + " has name " + name, e);
//		}
//	}
//
//
//	public static boolean hasOpt(ASTNode node, String optName) {
//		boolean isSet = false;
//		try {
//			Method m1 = node.getClass().getMethod("has" + optName);
//			 isSet = (boolean) m1.invoke(node);
//		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
//				| InvocationTargetException e) {
//			throw new As2Exception("Error in hasOpt: " + node + " has name: " + optName, e);
//		}
//		return isSet;
//	}
//	
//	public static Class<?>[] getClasses(ASTNode node, String name,SubclassExtractor sce) {
//		Class<?>[] classes = null;
//		try {
//			Method m = node.getClass().getMethod("get" + name);
//			classes = sce.getSubClasses(m.getReturnType());
//			
//		} catch (NoSuchMethodException | SecurityException | IllegalArgumentException e) {
//			throw new As2Exception("Error in getting classes from: " + node + " that has name " + name, e);
//		}
//		return classes;
//	}
//	
//	public static List<ASTNode> extractItems(ASTNode node, String listName) {
//		final List<ASTNode> list;
//		try {
//			Method m = node.getClass().getMethod("get" + listName + "List");
//			list = (List<ASTNode>) m.invoke(node);
//		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
//				| InvocationTargetException e) {
//			e.printStackTrace();
//			throw new As2Exception("Error in extracting items from: " + node + " in list named: " + listName, e);
//		}
//		return list;
//	}
//	
//	public static ASTNode extractItem(ASTNode node, String child) {
//		final ASTNode res;
//		try {
//			Method m = node.getClass().getMethod("get" + child);
//			res = (ASTNode) m.invoke(node);
//		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
//				| InvocationTargetException e) {
//			throw new As2Exception("Error in extract item on: " + node + " whith name: " + child, e);
//		}
//		return res;
//	}
//
//	public static Class<?>[] getClassesList(ASTNode node, String name, SubclassExtractor sce) {
//		Class<?>[] classes = null;
//		try {
//			Method m = node.getClass().getMethod("get" + name,int.class);
//			classes = sce.getSubClasses(m.getReturnType());
//			
//		} catch (NoSuchMethodException | SecurityException | IllegalArgumentException e) {
//			throw new As2Exception("Error in getting List: get" + name, e);
//		}
//		return classes;
//	}
//	
//	public static String getValue(ASTNode node,String token) {
//		try {
//			Method mot = node.getClass().getMethod("get" + token);
//			return (String) mot.invoke(node);
//		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
//				| InvocationTargetException e) {
//			e.printStackTrace();
//			throw new As2Exception("Can not find method: " + "get" + token, e);
//		}
//	}
//	
//	public static Class<?> getCurrentClass(ASTNode node,String name){
//		try {
//			Method m = node.getClass().getMethod("get" + name);
//			Object o = m.invoke(node);
//			return o.getClass();
//		} catch (NoSuchMethodException | SecurityException | IllegalArgumentException | IllegalAccessException | InvocationTargetException e) {
//			throw new As2Exception("Error in getting current class of: " + name + " in " + node, e);
//		}	
//	}
//
//	public static String getListNameFor(ASTNode node) {
//		if(!(node.getParent() instanceof List<?>)){
//			throw new As2Exception("Element " + node +  " not in list in: getListNameFor");
//		}
//		ASTNode parent = node.getParent();
//		ASTNode grandParent = parent.getParent();
//		String[] lists = Extractor.getList(grandParent.getClass());
//
//		String res = null;
//		for (int i = 0; i < lists.length; i++) {
//			String name = lists[i];
//			if(parent == extractItems(grandParent, name)){
//				res = name;
//			}
//		}
//		if(res == null){
//			throw new As2Exception("Element " + node +  " not in list in: getListNameFor");
//		}
//		return res;
//	}
//	
//	public static String getShowNameFor(ASTNode node) {
//		String res = getNameFor(node);
//		
//		String end;
//		if(node instanceof List){
//			end = "*";
//			res = res.substring(0,res.length() - 4);
//		} else if (node instanceof Opt){
//			end = "?";
//			res = res.substring(0,res.length() - 3);
//		} else {
//			end = ":" + node.getClass().getSimpleName();
//			
//		}
//		return res + end;
//	}
//
//	public static String getNameFor(ASTNode node) {
//		ASTNode parent = node.getParent();
//		String[] children = Extractor.getAll(parent.getClass());
//		String res = null;
//		for (int i = 0; i < children.length; i++) {
//			String name = children[i];
//			if(node == extractItem(parent, name)){
//				res = name;
//			}
//		}
//		if(res == null){
//			for(String s : children){
//				System.out.println(s);
//			}
//			throw new As2Exception("Can node get name for: " + node);
//		}
//		return res;
//	}
//	
//	public static int getListIndex(ASTNode node ) {
//		ASTNode list = node.getParent();
//		int res = -1;
//		for(int i = 0; i < list.getNumChild(); i++){
//			if(list.getChild(i) == node){
//				res = i;
//			}
//		}	
//		return res;
//	}
//
//
//	public static void changeElement(ASTNode oldNode, Class<?> toCreate , SubclassExtractor se) {
//		ASTNode parent = oldNode.getParent();
//		int index = getListIndex(oldNode);
//		ASTNode node;
//		try {
//			node = (ASTNode) toCreate.newInstance();
//			se.inferAll(node);
//			parent.setChild(node, index);
//		} catch (InstantiationException | IllegalAccessException e) {
//			throw new As2Exception("Error in changeching node: " + oldNode + " to node of type " + toCreate.getName(), e);
//		}
//		
//	}
//}

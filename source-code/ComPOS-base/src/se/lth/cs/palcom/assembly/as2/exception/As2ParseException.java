package se.lth.cs.palcom.assembly.as2.exception;

public class As2ParseException extends As2Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7089196479561884655L;

	public As2ParseException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public As2ParseException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public As2ParseException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public As2ParseException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public As2ParseException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}

//package se.lth.cs.palcom.assembly.as2;
//
//import java.io.BufferedInputStream;
//import java.io.BufferedReader;
//import java.io.File;
//import java.io.FileFilter;
//import java.io.FileNotFoundException;
//import java.io.FileReader;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.InputStreamReader;
//import java.lang.reflect.InvocationTargetException;
//import java.lang.reflect.Method;
//import java.util.ArrayList;
//import java.util.Comparator;
//import static se.lth.cs.palcom.assembly.as2.Extractor.*;
//
//import se.lth.cs.palcom.assembly.as2.exception.As2Exception;
//import se.lth.cs.palcom.assembly.as2.gen.ASTNode;
//
//public class SubclassExtractor {
//	private int count = 0;
//
//	private File path;
//
//	public SubclassExtractor(File path) {
//		this.path = path;
//	}
//
//	public Class<?>[] getSubClasses(Class<?> o) {
//		InputStream content = getClass().getResourceAsStream("/dir.txt");
//		BufferedReader is;
//		if (content != null) {
//			is = new BufferedReader(new InputStreamReader(content));
//		} else {
//			try {
//				is = new BufferedReader(new FileReader("../Assembly-as2-xml/dir.txt"));
//			} catch (FileNotFoundException e) {
//				throw new As2Exception(e);
//			}
//		}
//		ArrayList<Class<?>> res = new ArrayList<Class<?>>();
//		String pack = o.getPackage().getName();
//		try {
//			while (is.ready()) {
//				String fileName = is.readLine();
//					Object tmp;
//					try {
//						tmp = Class.forName(pack + "." + fileName.substring(0, fileName.length() - 5))
//								.newInstance();
//						if (o.isInstance(tmp)) {
//							res.add(tmp.getClass());
//						}
//					} catch (InstantiationException e) {
//					} catch (IllegalAccessException e) {
//					} catch (ClassNotFoundException e) {
//					}
//					
//			}
//		} catch (IOException e1) {
//				throw new As2Exception(e1);
//		}
//		return res.toArray(new Class<?>[0]);
//	}
//
//	public void inferAll(ASTNode node) {
//		for (int i = 1; i < 10; i++) {
//			if (inferAll(node, i)) {
//				return;
//			}
//
//		}
//		throw new As2Exception("To complex abstract grammar can not infer all members");
//
//	}
//
//	private boolean inferAll(ASTNode node, int level) {
//		if (level == 0) {
//			return false;
//		}
//
//		for (String childName : getChild(node.getClass())) {
//			if (!canCreateChild(childName, node, level)) {
//				return false;
//			}
//		}
//		fillTokens(node);
//
//		return true;
//
//	}
//
//	private boolean canCreateChild(String childName, ASTNode node, int level) {
//		Class<?>[] typesOfChildren = getTypesOfChildren(childName, node);
//
//		ArrayList<ASTNode> childCandidates = getChildCandidates(typesOfChildren);
//
//		for (ASTNode child : childCandidates) {
//			if (getChild(child.getClass()).length == 0) {
//				fillTokens(child);
//				setValue(node, childName, child);
//				return true;
//
//			} else {
//				boolean res = inferAll(child, level - 1);
//				if (res) {
//					fillTokens(child);
//					setValue(node, childName, child);
//					return true;
//				}
//
//			}
//
//		}
//		return false;
//	}
//
//	private ArrayList<ASTNode> getChildCandidates(Class<?>[] typesOfChildren) {
//		ArrayList<ASTNode> childCandidates = new ArrayList<ASTNode>();
//		for (Class<?> c : typesOfChildren) {
//			try {
//				childCandidates.add((ASTNode) c.newInstance());
//			} catch (InstantiationException e) {
//				throw new As2Exception("Can not create instance of: " + c.getName(), e);
//			} catch (IllegalAccessException e) {
//				throw new As2Exception("Can not create instance of: " + c.getName(), e);
//			}
//		}
//		childCandidates.sort(new Comparator<ASTNode>() {
//			@Override
//			public int compare(ASTNode o1, ASTNode o2) {
//				int child = Integer.compare(getChild(o1.getClass()).length, getChild(o2.getClass()).length);
//				if (child != 0) {
//					return child;
//				}
//				int token = Integer.compare(getTokens(o1.getClass()).length, getTokens(o2.getClass()).length);
//				if (token != 0) {
//					return token;
//				}
//				int list = Integer.compare(getList(o1.getClass()).length, getList(o2.getClass()).length);
//				if (list != 0) {
//					return list;
//				}
//				return Integer.compare(getOpt(o1.getClass()).length, getOpt(o2.getClass()).length);
//			}
//
//		});
//		return childCandidates;
//	}
//
//	private void setValue(ASTNode node, String childName, ASTNode child) {
//		Method m;
//		try {
//			m = node.getClass().getMethod("set" + childName, getReturnType(childName, node));
//			m.invoke(node, child); // Maybe have a default value
//		} catch (NoSuchMethodException e) {
//			throw new As2Exception("Can not call method: set" + childName, e);
//		} catch (SecurityException e) {
//			throw new As2Exception("Can not call method: set" + childName, e);
//		} catch (IllegalAccessException e) {
//			throw new As2Exception("Can not call method: set" + childName, e);
//		} catch (IllegalArgumentException e) {
//			throw new As2Exception("Can not call method: set" + childName, e);
//		} catch (InvocationTargetException e) {
//			throw new As2Exception("Can not call method: set" + childName, e);
//		}
//	}
//
//	private void fillTokens(ASTNode child) {
//		for (String token : getTokens(child.getClass())) {
//			try {
//				String fillIn = child.ed_default_token(token);
//				Method m = child.getClass().getMethod("set" + token, String.class);
//				m.invoke(child, "Fill in");
//			} catch (NoSuchMethodException e) {
//				throw new As2Exception("Can not call method: set" + token, e);
//			} catch (SecurityException e) {
//				throw new As2Exception("Can not call method: set" + token, e);
//			} catch (IllegalAccessException e) {
//				throw new As2Exception("Can not call method: set" + token, e);
//			} catch (IllegalArgumentException e) {
//				throw new As2Exception("Can not call method: set" + token, e);
//			} catch (InvocationTargetException e) {
//				throw new As2Exception("Can not call method: set" + token, e);
//			} catch (Exception e) {
//				throw new As2Exception("Can not call method: set: Other problem" + token, e);
//			}
//
//		}
//
//	}
//
//	private Class<?>[] getTypesOfChildren(String childName, ASTNode node) {
//		return getSubClasses(getReturnType(childName, node));
//	}
//
//	private Class<?> getReturnType(String childName, ASTNode node) {
//		try {
//			Method m = node.getClass().getMethod("get" + childName);
//			Class<?> type = m.getReturnType();
//			return type;
//		} catch (NoSuchMethodException e) {
//			throw new As2Exception("Can not find mentod get" + childName, e);
//		} catch (SecurityException e) {
//			throw new As2Exception("Can not find mentod get" + childName, e);
//		}
//	}
//
//}

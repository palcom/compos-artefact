//package se.lth.cs.palcom.assembly.as2;
//
//import java.util.ArrayList;
//import java.util.Observable;
//
//import se.lth.cs.palcom.assembly.as2.gen.ASTNode;
//import se.lth.cs.palcom.device.AbstractDevice;
//
//public class CompilerContainer extends Observable{
//	
//	
//	
//	private SubclassExtractor se;
//	private AbstractDevice device;
//	private ArrayList<ASTNode<?>> changedNodes;
//	public CompilerContainer(SubclassExtractor se) {
//		this.se = se;
//		 changedNodes = new ArrayList<>();
//	}
//
//	public CompilerContainer(SubclassExtractor se2, AbstractDevice device) {
//		this(se2);
//		this.device = device;
//	}
//
//	public void hasChangeTree() {
//		setChanged();
//		notifyObservers();
//		changedNodes.clear();
//	}
//
//	public SubclassExtractor getSubclassExtractor() {
//		return se;
//	}
//	
//	public AbstractDevice getDevice() {
//		return device;
//	}
//	
//	public void addChangedNode(ASTNode<?> node){
//		changedNodes.add(node);
//	}
//	
//	public ArrayList<ASTNode<?>> getChangesNodes(){
//		return changedNodes;
//	}
//	
//
//}

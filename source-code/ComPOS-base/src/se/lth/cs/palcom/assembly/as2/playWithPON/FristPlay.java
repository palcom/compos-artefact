package se.lth.cs.palcom.assembly.as2.playWithPON;

import se.lth.cs.palcom.pon.JSONConverter;
import se.lth.cs.palcom.pon.PON;
import se.lth.cs.palcom.pon.PONWriter;
import se.lth.cs.palcom.pon.schema.ClassSchemaBuilder;
import se.lth.cs.palcom.pon.schema.PONSchema;
import se.lth.cs.palcom.pon.schema.PrimitiveType.ArrayType;
import se.lth.cs.palcom.pon.schema.PrimitiveType.BinaryType;
import se.lth.cs.palcom.pon.schema.PrimitiveType.BoolType;
import se.lth.cs.palcom.pon.schema.PrimitiveType.ByteType;
import se.lth.cs.palcom.pon.schema.PrimitiveType.CharType;
import se.lth.cs.palcom.pon.schema.PrimitiveType.DoubleType;
import se.lth.cs.palcom.pon.schema.PrimitiveType.FloatType;
import se.lth.cs.palcom.pon.schema.PrimitiveType.IntType;
import se.lth.cs.palcom.pon.schema.PrimitiveType.LongType;
import se.lth.cs.palcom.pon.schema.PrimitiveType.ObjectType;
import se.lth.cs.palcom.pon.schema.PrimitiveType.StringType;
import se.lth.cs.palcom.pon.schema.Schema;
import se.lth.cs.palcom.pon.schema.SchemaTraverser;
import se.lth.cs.palcom.pon.schema.SchemaTraverserCallback;
import se.lth.cs.palcom.pon.schema.SchemaBuilder.ObjectBuilder;
import se.lth.cs.palcom.pon.schema.SchemaBuilder.TypeBuilder;
import se.lth.cs.palcom.pon.schema.SimpleSchemaBuilder;
import se.lth.cs.palcom.pon.schema.TypeSchemaBuilder;

public class FristPlay {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TypeSchemaBuilder sb = PONSchema.typeSchemaBuilder("alfred");
		TypeBuilder s = sb.newType().id("Hello").def(sb.Object().property("najs", sb.String())).create();
		System.out.println(s.buildTypeString());
		
		System.out.println(s.getType());
		System.out.println(s.getDefinition().getType());
		
		String pon = "963{SchemaFormatVersion:85{name:4sv0.1update:64{refType:7sVersionid:20sA:mattiasn.cs.lth.separt:13sschema-170314type:6sschemakind:5sclassnamespace:13sknown-classesid:63sse.lth.cs.palcom.examples.pinger.EchoServiceBoxed$StringBoxPairdesc:31sSchema for class StringBoxPair.def:688{type:6sobjectproperties:660{s1:323{SchemaFormatVersion:85{name:4sv0.1update:64{refType:7sVersionid:20sA:mattiasn.cs.lth.separt:13sschema-170314type:6sschemakind:5sclassnamespace:13sknown-classesid:59sse.lth.cs.palcom.examples.pinger.EchoServiceBoxed$StringBoxdesc:27sSchema for class StringBox.def:57{type:6sobjectproperties:30{str:23{type:6sstringminLen:1i1s2:323{SchemaFormatVersion:85{name:4sv0.1update:64{refType:7sVersionid:20sA:mattiasn.cs.lth.separt:13sschema-170314type:6sschemakind:5sclassnamespace:13sknown-classesid:59sse.lth.cs.palcom.examples.pinger.EchoServiceBoxed$StringBoxdesc:27sSchema for class StringBox.def:57{type:6sobjectproperties:30{str:23{type:6sstringminLen:1i1";
		pon =  "1094{deviceID:38sC:e0d6b4ca-73df-4a1f-8685-01154942128epublishedRole:usandbox:1016{ConfigFormatVersion:96{name:4sv0.1update:75{refType:11sversionPartdeviceID:20sA:mattiasn.cs.lth.separtID:11sconf-170314devConfTypeVersionName:341{kind:12sdeviceConfigtypeID:173{kind:12sdeviceConfigreadableName:14sunnamed configuniqueRef:89{refType:8scompounddeviceID:38sC:e0d6b4ca-73df-4a1f-8685-01154942128epartID:11sa799-190625fileSuffix:8s.devconfversion:125{name:11sunversionedupdate:96{refType:11sversionPartdeviceID:38sC:e0d6b4ca-73df-4a1f-8685-01154942128epartID:14svp-cf34-190625resources:515[511{kind:8sassemblytypeVersionRole:476{typeVersionReference:434{kind:8sassemblytypeID:160{kind:8sassemblyreadableName:10sSimpleEchouniqueRef:89{refType:8scompounddeviceID:38sC:e0d6b4ca-73df-4a1f-8685-01154942128epartID:11s6181-190628fileSuffix:4s.as2version:222{name:10sSimpleEchoupdate:93{refType:11sversionPartdeviceID:38sC:e0d6b4ca-73df-4a1f-8685-01154942128epartID:11sf688-190628base:93{refType:11sversionPartdeviceID:38sC:e0d6b4ca-73df-4a1f-8685-01154942128epartID:11sf688-190628contentHash:0srole:1s1enabled:t";
		System.out.println(new String(JSONConverter.ponToJson(pon.getBytes())));
	}

}

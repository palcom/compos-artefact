package se.lth.cs.palcom.analysis;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.jastadd.tree.edit.xml.JastAddXMLParser;

import internal.org.xmlpull.v1.XmlPullParserException;
import ist.palcom.resource.descriptor.FullServiceAddress;
import naming.TypeVersion;
import naming.assembly.AssemblyInstanceReference;
import naming.device.DeviceInstanceReference;
import naming.service.ServiceInstanceReference;
import se.lth.cs.palcom.analysis.gen.As2Unit;
import se.lth.cs.palcom.analysis.gen.CompositionInstance;
import se.lth.cs.palcom.analysis.gen.DeviceHandle;
import se.lth.cs.palcom.analysis.gen.DeviceInstance;
import se.lth.cs.palcom.analysis.gen.List;
import se.lth.cs.palcom.analysis.gen.NativeServiceInstance;
import se.lth.cs.palcom.analysis.gen.Opt;
import se.lth.cs.palcom.analysis.gen.Pair;
import se.lth.cs.palcom.analysis.gen.Root;
import se.lth.cs.palcom.analysis.gen.ServiceHandle;
import se.lth.cs.palcom.analysis.gen.ServiceVariable;
import se.lth.cs.palcom.analysis.gen.SynthesizedServiceInstance;
import se.lth.cs.palcom.discovery.DeviceListener;
import se.lth.cs.palcom.discovery.ResourceException;
import se.lth.cs.palcom.discovery.proxy.PalcomConnection;
import se.lth.cs.palcom.discovery.proxy.PalcomConnectionList;
import se.lth.cs.palcom.discovery.proxy.PalcomDevice;
import se.lth.cs.palcom.discovery.proxy.PalcomService;
import se.lth.cs.palcom.discovery.proxy.PalcomServiceList;
import se.lth.cs.palcom.discovery.proxy.PalcomServiceListPart;
import se.lth.cs.palcom.pon.JSONConverter;
import se.lth.cs.palcom.pon.PON;
import se.lth.cs.palcom.pon.PONException;
import se.lth.cs.palcom.resourcemanager.ResourceManager;

public class ModelDeviceListener implements DeviceListener {

	private final Root tree;
	private ResourceManager rm;

	public ModelDeviceListener(Root tree, ResourceManager rm) {
		this.tree = tree;
		this.rm = rm;

	}

	public Root getTree() {
		return tree;
	}

	@Override
	public void deviceRemoved(PalcomDevice device) {
		try {
			synchronized (tree) {
				deviceRemoved2(device);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void deviceRemoved2(PalcomDevice device) {
		System.out.println("Device removed");
		int index;
		index = tree.getPalcomSystem().findDeviceIndex(device.getDeviceID().getID());
		if (index >= 0) {
			DeviceHandle dh = tree.getPalcomSystem().getDeviceHandle(index);
			if (dh.hasDeviceInstance()) {
				DeviceInstance di = dh.getDeviceInstance();
				for (CompositionInstance c : di.getCompositionInstances()) {
					c.composition().removeFromUses(c);
					for (ServiceHandle cts : c.connectsTo()) {
						c.removeFromConnectsTo(cts);
					}
				}
				for (NativeServiceInstance nsi : di.getNativeServiceInstances()) {
					nsi.serviceHandle().clearServiceInstance();
				}
			}

			System.out.println("Device removed");
			dh.setDeviceInstanceOpt(new Opt<>());
			tree.flushTreeCache();
		}

	}

	@Override
	public void deviceChanged(PalcomDevice device) {
		try {
			synchronized (tree) {
				deviceChanged2(device);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void deviceChanged2(PalcomDevice device) {
		System.out.println("Device change:" + device.getDeviceID().getID());
		// deviceRemoved(device);
		// deviceAdded(device);
		DeviceHandle dh = tree.getPalcomSystem().findDeviceHandel(device.getDeviceID().getID());
		if (dh == null || !dh.hasDeviceInstance()) {
			deviceAdded(device);
			return;
		}
		DeviceInstance di = dh.getDeviceInstance();
		ArrayList<ServiceInstanceReference> sirl = new ArrayList<>();
		ArrayList<AssemblyInstanceReference> airl = new ArrayList<>();
		fillWithInfoFromDevice(device, sirl, airl);
		ArrayList<ServiceInstanceReference> sirlWithSynth = (ArrayList<ServiceInstanceReference>) sirl.clone();

		filterSynth(sirl, airl);

		System.out.println("%AA Size sirwithlsynthe " + sirlWithSynth.size());

		// Adde non exisisting compostitions
		for (AssemblyInstanceReference air : airl) {
			if (di.findComposition(air) == null) {
				List<SynthesizedServiceInstance> ssil = new List<>();
				fillSyntService(dh, sirlWithSynth, air, ssil);
				CompositionInstance tmp = createCompositionInstance(air, ssil);
				System.out.print("%AA här är det fel: ");
				System.out.println(air);
				System.out.println(ssil.getNumChild());
				di.addCompositionInstance(tmp);
			} else {
				CompositionInstance ref = di.findComposition(air);
				updateWithSyntService(dh, sirlWithSynth, air, ref);
			}
		}

		// Remove removed compsitions
		for (int i = di.getNumCompositionInstance() - 1; i >= 0; i--) {
			CompositionInstance ci = di.getCompositionInstance(i);
			if (!airl.contains(ci.getAssemblyInstanceReference())) {
				while (ci.connectsTo().size() > 0) {
					ci.removeFromConnectsTo(ci.connectsTo().get(0));
				}
				ci.composition().removeFromUses(ci);
				di.getCompositionInstanceList().removeChild(i);
			}

		}

		// Add extra service instance reference
		for (ServiceInstanceReference sir : sirl) {
			if (di.findService(sir) == null) {
				ServiceHandle sh = findOrAddUniqueServiceHandle(dh, sir);
				NativeServiceInstance nsi = new NativeServiceInstance(PON.toPONString(sir));
				di.addNativeServiceInstance(nsi);
				sh.setServiceInstance(nsi);
			}
		}

		// Remove remoced native services
		for (int i = di.getNumNativeServiceInstance() - 1; i >= 0; i--) {
			NativeServiceInstance nsi = di.getNativeServiceInstance(i);
			if (!sirl.contains(nsi.getServiceInstanceReference())) {
				nsi.serviceHandle().clearServiceInstance();
				di.getNativeServiceInstanceList().removeChild(i);
			}
		}

		tree.flushTreeCache();

		ArrayList<Pair<AssemblyInstanceReference, ServiceInstanceReference>> pairList = findConnections(device);
		// Add new connections
		for (Pair<AssemblyInstanceReference, ServiceInstanceReference> pari : pairList) {
			CompositionInstance comp = di.findComposition(pari._1);
			if (comp == null) {
				// Do Noting
				System.out.println("%AA Did not find: " + pari._1);
			} else if (!comp.doConnectsTo(pari._2)) {
				ServiceHandle sh = findOrAddUniqueServiceAndDevice(tree, pari._2);
				comp.addToConnectsTo(sh);
			}
		}

		// Remove connections
		for (CompositionInstance comp : di.getCompositionInstances()) {
			for (int i = comp.connectsTo().size() - 1; i >= 0; i--) {
				AssemblyInstanceReference air = comp.getAssemblyInstanceReference();
				ServiceInstanceReference sir = comp.connectsTo().get(i).getServiceInstanceReference();
				if (!pairList.contains(new Pair<>(air, sir))) {
					comp.removeFromConnectsTo(comp.connectsTo().get(i));
				}
			}
		}

		tree.flushTreeCache();

	}

	private CompositionInstance createCompositionInstance(AssemblyInstanceReference air,
			List<SynthesizedServiceInstance> ssil) {
		As2Unit as2 = tree.getStaticInfo().findUnit(air.getAssemblyType());
		if (as2 == null) {
			TypeVersion tv = rm.getResource(air.getAssemblyType());
			if (tv != null) {
				try {
					as2 = (As2Unit) JastAddXMLParser.parse(new String(tv.getContent()),
							"se.lth.cs.palcom.analysis.gen");
					tree.getStaticInfo().addAs2Unit(as2);
				} catch (XmlPullParserException e) {
					e.printStackTrace();
					as2 = null;
				} catch (IOException e) {
					e.printStackTrace();
					as2 = null;
				}
			} else {
				throw new RuntimeException(" Composition not in local store, may be due to unpublish version.");
			}
		}

		for (ServiceVariable tmp : as2.getAs2Assembly().getBindings().getServiceVariables()) {
			if (!tmp.device().isLocal()) {
				findOrAddUniqueServiceAndDevice(tree, tmp.getServiceInstanceReference());
			}
		}

		CompositionInstance tmp = new CompositionInstance(PON.toPONString(air), ssil);
		if (as2 != null) {
			tmp.setComposition(as2);
		}
		return tmp;
	}

	@Override
	public void deviceAdded(PalcomDevice device) {

		try {
			synchronized (tree) {
				deviceAdded2(device);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deviceAdded2(PalcomDevice device) {
		try {
			device.waitForAvailable(400);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}

		// DeviceInstance deviceInstance = new DeviceInstance(
		// PON.toPONString(DeviceInstanceReference.convert(device)), sil, cil);
		DeviceHandle dh = findOrAddUniqueDeviceHandel(tree, device.getDeviceID().getID());

		System.out.println("Device added");
		ArrayList<ServiceInstanceReference> sirl = new ArrayList<>();
		ArrayList<AssemblyInstanceReference> airl = new ArrayList<>();
		fillWithInfoFromDevice(device, sirl, airl);

		// Find the pairs of Sevice instances matching stuff

		List<CompositionInstance> cil = new List<>();

		filterSIRandAddComp(dh, sirl, airl, cil);

		List<NativeServiceInstance> sil = new List<>();

		for (ServiceInstanceReference ssi : sirl) {
			NativeServiceInstance ns = new NativeServiceInstance(PON.toPONString(ssi));
			sil.add(ns);
			ServiceHandle sh = findOrAddUniqueServiceHandle(dh, ssi);
			sh.setServiceInstance(ns);
		}
		DeviceInstance deviceInstance;
		try {
			deviceInstance = new DeviceInstance(PON.toPONString(DeviceInstanceReference.convert(device)), sil, cil);
			dh.setDeviceInstance(deviceInstance);
		} catch (PONException e) {
			e.printStackTrace();
			return;
		} catch (ResourceException e) {
			e.printStackTrace();
			return;
		}
		tree.flushTreeCache();

		ArrayList<Pair<AssemblyInstanceReference, ServiceInstanceReference>> pairList = findConnections(device);

		Iterator<Pair<AssemblyInstanceReference, ServiceInstanceReference>> itr2 = pairList.iterator();
		while (itr2.hasNext()) {
			Pair<AssemblyInstanceReference, ServiceInstanceReference> pair = itr2.next();
			AssemblyInstanceReference potentialComposition = pair._1;
			ServiceInstanceReference potentialService = pair._2;
			CompositionInstance comp = deviceInstance.findComposition(potentialComposition);
			if (comp != null) {
				itr2.remove(); // Shall work if there is no service;
				ServiceHandle sh = findOrAddUniqueServiceAndDevice(tree, potentialService);
				comp.addToConnectsTo(sh);
			}
		}
	}

	private ServiceHandle findOrAddUniqueServiceAndDevice(Root tree, ServiceInstanceReference potentialService) {
		String potientialServicedeviceId = potentialService.getContainingDevice().getInstanceID().getUniqueName();
		DeviceHandle tmpDh = findOrAddUniqueDeviceHandel(tree, potientialServicedeviceId);
		ServiceHandle sh = findOrAddUniqueServiceHandle(tmpDh, potentialService);
		return sh;
	}

	private ArrayList<Pair<AssemblyInstanceReference, ServiceInstanceReference>> findConnections(PalcomDevice device) {
		PalcomConnectionList cl = device.getConnectionList();
		ArrayList<Pair<AssemblyInstanceReference, ServiceInstanceReference>> pairList = new ArrayList<>();
		try {
			cl.waitForAvailable(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		for (int i = 0; i < cl.getNumConnection(); i++) {
			try {

				PalcomConnection pc = cl.getConnection(i);

				if (pc != null && pc.getProviderAddress() instanceof FullServiceAddress
						&& pc.getCustomerAddress() instanceof FullServiceAddress) {
					FullServiceAddress provider = (FullServiceAddress) pc.getProviderAddress(); // Customer
																								// ansluter till
																								// en provider
					FullServiceAddress consumer = (FullServiceAddress) pc.getCustomerAddress(); // Sätter upp
																								// connection
					ServiceInstanceReference providerSIR = ServiceInstanceReference
							.convert(provider.getServiceInstanceID());
					AssemblyInstanceReference consumerSIR = AssemblyInstanceReference
							.convert(ServiceInstanceReference.convert(consumer.getServiceInstanceID()));
					pairList.add(
							new Pair<AssemblyInstanceReference, ServiceInstanceReference>(consumerSIR, providerSIR));
				}
			} catch (Exception e) {
				System.out.println("Is there an error we just try the next");
				e.printStackTrace();
				// It there is an error we just try the next
			}

		}

		return pairList;
	}

	private void filterSIRandAddComp(DeviceHandle dh, ArrayList<ServiceInstanceReference> sirl,
			ArrayList<AssemblyInstanceReference> airl, List<CompositionInstance> cil) {
		for (AssemblyInstanceReference air : airl) {
			System.out.println("%AA air " + air.getTypeVersionReference().getUniqueName());
			List<SynthesizedServiceInstance> ssil = new List<>();
			fillSyntService(dh, sirl, air, ssil);
			cil.add(createCompositionInstance(air, ssil));
		}
	}

	private void fillSyntService(DeviceHandle dh, ArrayList<ServiceInstanceReference> sirl,
			AssemblyInstanceReference air, List<SynthesizedServiceInstance> ssil) {
		Iterator<ServiceInstanceReference> itr = sirl.iterator();
		while (itr.hasNext()) {
			ServiceInstanceReference sir = itr.next();
			if (hackSame(air, sir)) {
				SynthesizedServiceInstance ssi = new SynthesizedServiceInstance(PON.toPONString(sir));
				ssil.add(ssi);
				ServiceHandle sh = findOrAddUniqueServiceHandle(dh, sir);
				sh.setServiceInstance(ssi);
				System.out.println("%AA Add Synth ");
				itr.remove();
			}
		}
	}

	private void updateWithSyntService(DeviceHandle dh, ArrayList<ServiceInstanceReference> sirl,
			AssemblyInstanceReference air, CompositionInstance ci) {
		Iterator<ServiceInstanceReference> itr = sirl.iterator();
		while (itr.hasNext()) {
			ServiceInstanceReference sir = itr.next();
			if (hackSame(air, sir)) {
				ServiceHandle sh = findOrAddUniqueServiceHandle(dh, sir);
				if (!sh.hasServiceInstance()) {
					SynthesizedServiceInstance ssi = new SynthesizedServiceInstance(PON.toPONString(sir));
					ci.addSynthesizedServiceInstance(ssi);
					sh.setServiceInstance(ssi);
					System.out.println("%AA Add Synth ");
					itr.remove();
				}
			}
		}
	}

	private void filterSynth(ArrayList<ServiceInstanceReference> sirl, ArrayList<AssemblyInstanceReference> airl) {
		for (AssemblyInstanceReference air : airl) {
			Iterator<ServiceInstanceReference> itr = sirl.iterator();
			while (itr.hasNext()) {
				ServiceInstanceReference sir = itr.next();
				boolean hackSame = hackSame(air, sir);
				if (hackSame) { // The assembly and the synt
					// service allway has the same
					// version
					itr.remove();
				}
			}

		}
	}

	private boolean hackSame(AssemblyInstanceReference air, ServiceInstanceReference sir) {
		// This is used in the hack to find if sir represent the same air
		boolean sameSersion = sir.getTypeVersionReference().getVersion()
				.equals(air.getTypeVersionReference().getVersion());
		boolean sameRole = sir.getRole().getReadableName().equals(air.getRole().getReadableName());
		System.out.println("sr" + sameRole + " ss " + sameRole);
		boolean hackSame = sameSersion && sameRole;
		return hackSame;
	}

	private void fillWithInfoFromDevice(PalcomDevice device, ArrayList<ServiceInstanceReference> sirl,
			ArrayList<AssemblyInstanceReference> airl) {
		PalcomServiceList sl = device.getServiceList();
		System.out.println("nbr service:" + sl.getNumService());
		for (int i = 0; i < sl.getNumService(); i++) {
			PalcomServiceListPart slp = sl.getService(i);
			if (slp instanceof PalcomService) {
				PalcomService sp = (PalcomService) slp;
				try {
					sp.waitForAvailable(100);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				try {
					if (sp.hasDescription()) {
						try {
							ServiceInstanceReference sir = ServiceInstanceReference.convert(sp);
							sirl.add(sir);
							System.out.println("%AA add service: " + sir);
						} catch (ResourceException e) {
							System.out.println("continue service");
						}
					} else if (sp.getHelpText().equals("AlfredUglyHack")) {
						AssemblyInstanceReference air = AssemblyInstanceReference.convert(sp);
						System.out.println("%AA add composition: " + air);
						airl.add(air);
					} else {
						System.out.println("No desp for" + sp);
					}
				} catch (ResourceException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private DeviceHandle findOrAddUniqueDeviceHandel(Root tree, String potientialServicedeviceId) {
		DeviceHandle tmpDh = tree.getPalcomSystem().findDeviceHandel(potientialServicedeviceId);
		if (tmpDh == null) {
			tmpDh = new DeviceHandle(potientialServicedeviceId, new Opt<>(), new List<>());
			tree.getPalcomSystem().addDeviceHandle(tmpDh);
		}
		return tmpDh;
	}

	private ServiceHandle findOrAddUniqueServiceHandle(DeviceHandle dh, ServiceInstanceReference sir) {
		ServiceHandle sh = dh.findServiceHandle(sir);
		if (sh == null) {
			sh = new ServiceHandle(PON.toPONString(sir));
			dh.addServiceHandle(sh);
		}
		return sh;
	}
}
package se.lth.cs.palcom.analysis.gui;

import java.awt.GridLayout;
import java.awt.Label;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DropTarget;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;

import naming.assembly.AssemblyInstanceReference;
import naming.device.DeviceInstanceReference;
import naming.service.ServiceInstanceReference;
import se.lth.cs.palcom.analysis.datastuctures.Explored;
import se.lth.cs.palcom.analysis.datastuctures.SetOfSet;
import se.lth.cs.palcom.analysis.gen.Root;
import se.lth.cs.palcom.analysis.gen.SmallSet;
import se.lth.cs.palcom.browsergui.dnd.CommandWrapper;
import se.lth.cs.palcom.browsergui.dnd.ResourceTransferable;
import se.lth.cs.palcom.browsergui.dnd.ServiceCommandWrapper;
import se.lth.cs.palcom.device.AbstractDevice;
import se.lth.cs.palcom.discovery.DeviceListener;
import se.lth.cs.palcom.discovery.ResourceException;
import se.lth.cs.palcom.discovery.proxy.PalcomDevice;

public class DDAPanel extends JPanel {

	private AbstractDevice device;
	private Root tree;
	private JTextArea jf;

	public DDAPanel(AbstractDevice device, Root tree, JTextArea jf) {
		super();
		this.device = device;
		this.tree = tree;
		this.jf = jf;

		setLayout(new GridLayout(0, 2));

		add(new JLabel("Command:"));
		DropCommandArea dca = new DropCommandArea();
		add(dca);
		add(new JLabel("Composition:"));
		// CompositionComboBox ccb = new CompositionComboBox();
		// add(ccb);

		JButton analyse = new JButton("Analyse");
		add(analyse);
		analyse.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// AssemblyInstanceReference air = (AssemblyInstanceReference)
				// ccb.getSelectedItem();
				ServiceInstanceReference sir;
				String command = dca.scw.command.getID();
				try {
					sir = dca.scw.getServiceInstanceReference();
				} catch (ResourceException e1) {
					e1.printStackTrace();
					return;
				}
				SetOfSet<Explored<DeviceInstanceReference>> res = new SetOfSet<>();
				synchronized (tree) {
					for (AssemblyInstanceReference air : tree.allAIR()) {
						res = res.union(tree.analyzeDeviceDependency(sir, air, command));
					}
				}

				jf.setText(buidString(res));

			}

			private String buidString(SetOfSet<Explored<DeviceInstanceReference>> res) {
				StringBuilder sb = new StringBuilder();
				for (SmallSet<Explored<DeviceInstanceReference>> dis : res) {
					sb.append("{ ");
					for (Explored<DeviceInstanceReference> dir : dis) {
						sb.append(dir.getBase().getReadableName());
						if (!dir.isExplored()) {
							sb.append('?');
						}
						sb.append(" ");
					}
					sb.append("}\n");

				}
				return sb.toString();
			}
		});

		device.getDiscoveryManager().addDeviceListener(new DeviceListener() {
			@Override
			public void deviceRemoved(PalcomDevice device) {
		//		ccb.reeval();
			}

			@Override
			public void deviceChanged(PalcomDevice device) {
		//		ccb.reeval();
			}

			@Override
			public void deviceAdded(PalcomDevice device) {
		//		ccb.reeval();
			}
		});

	}

	private class DropCommandArea extends JLabel {
		ServiceCommandWrapper scw;

		public DropCommandArea() {
			setText("Drag here");
			setTransferHandler(new CommandTransferHanler());
		}

		private class CommandTransferHanler extends TransferHandler {

			@Override
			public boolean canImport(TransferSupport support) {
				return support.isDataFlavorSupported(new DataFlavor(ServiceCommandWrapper.class, "Resource"));
			}

			@Override
			public boolean importData(TransferSupport support) {
				try {
					scw = (ServiceCommandWrapper) support.getTransferable()
							.getTransferData(new DataFlavor(ServiceCommandWrapper.class, "Resource"));
				} catch (UnsupportedFlavorException | IOException e1) {
					e1.printStackTrace();
					return false;
				}
				SwingUtilities.invokeLater(new Runnable() {

					@Override
					public void run() {
						try {
							setText(scw.getServiceInstanceReference().getReadableName());
						} catch (ResourceException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
				return true;
			}

		}

	}

	private class CompositionComboBox extends JComboBox<AssemblyInstanceReference> {

		public CompositionComboBox() {
			synchronized (tree) {

				for (AssemblyInstanceReference air : tree.allAIR()) {
					addItem(air);
				}
			}
		}

		private void reeval() {
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					removeAllItems();
					synchronized (tree) {
						for (AssemblyInstanceReference air : tree.allAIR()) {
							addItem(air);
						}
					}
				}
			});

		}
	}

}

package se.lth.cs.palcom.analysis.datastuctures;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.function.Function;

import se.lth.cs.palcom.analysis.gen.SmallSet;
import se.lth.cs.palcom.service.command.SmallPackagingManager;

public class SetOfSet<T> implements Iterable<SmallSet<T>> {

	private ArrayList<SmallSet<T>> list;

	public SetOfSet() {
		this.list = new ArrayList<SmallSet<T>>();
	}
	
	public boolean isEmpty() {
		return list.isEmpty();
	}
	
	
	
	public static <T1> SetOfSet<T1> empty() {
		return new SetOfSet<T1>();
	}

	@Override
	public String toString() {
		return "SetOfSet [list=" + list + "]";
	}

	public SetOfSet<T> addToAll(T item) {
		if(list.isEmpty()) {
			list.add(SmallSet.empty());
		}
		
		SetOfSet<T> res = new SetOfSet<>();
		
		for (SmallSet<T> s : this) {
			if (!res.list.contains(s.union(item))) {
				res.list.add(s.union(item));
			}
		}
		return res;
	}
	
	public SetOfSet<T> product(SetOfSet<T> set) {
		SetOfSet<T> res = new SetOfSet<>();
		if(list.isEmpty()) {
			return set;
		}
		if(set.list.isEmpty()) {
			return this;
		}
		
		for(SmallSet<T> a : this) {
			for(SmallSet<T> b : set) {
				res.list.add(a.union(b));
			}
		}
		return res;
		
	}

	public SetOfSet<T> union(SetOfSet<T> setOfSet) {
		SetOfSet<T> res = new SetOfSet<>();
		res.list.addAll(setOfSet.list);
		for (SmallSet<T> s : this) {
			if (!res.list.contains(s)) {
				res.list.add(s);
			}
		}
		return res;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SetOfSet<T> other = (SetOfSet<T>) obj;
		if (list == null) {
			if (other.list != null)
				return false;
		}
		if (this.list.size() != other.list.size()) {
			return false;
		}
		if (!this.list.containsAll(other.list)) {
			return false;
		}
		return true;
	}

	@Override
	public Iterator<SmallSet<T>> iterator() {
		return list.iterator();
	}

	public SetOfSet<T> add(SmallSet<T> s) {
		SetOfSet<T> res = new SetOfSet<>();
		res.list.addAll(this.list);
		if (!res.list.contains(s)) {
			res.list.add(s);
		}
		return res;
	}
	
	public <R> SetOfSet<R> map(Function<T, R> f){
		SetOfSet<R> res = new SetOfSet<>();
		for(SmallSet<T> ss : this) {
			SmallSet<R> tmp = SmallSet.<R>mutable();
			for(T el : ss) {
				tmp.add(f.apply(el));
			}
			res = res.add(tmp);
		}
		return res;
		
	}

}

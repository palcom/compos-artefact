package se.lth.cs.palcom.analysis.datastuctures;

import se.lth.cs.palcom.analysis.gen.ServiceHandle;

public class Dependency {
	private final ServiceHandle sh;
	private final String message;
	private final boolean resolved;
	public Dependency(ServiceHandle sh, String message, boolean resolved) {
		super();
		this.sh = sh;
		this.message = message;
		this.resolved = resolved;
	}
	public ServiceHandle getSh() {
		return sh;
	}
	public String getMessage() {
		return message;
	}
	public boolean isResolved() {
		return resolved;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		result = prime * result + (resolved ? 1231 : 1237);
		result = prime * result + ((sh == null) ? 0 : sh.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dependency other = (Dependency) obj;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		if (resolved != other.resolved)
			return false;
		if (sh == null) {
			if (other.sh != null)
				return false;
		} else if (!sh.equals(other.sh))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Dependency [sh=" + sh + ", message=" + message + ", resolved=" + resolved + "]";
	}
	
	

}

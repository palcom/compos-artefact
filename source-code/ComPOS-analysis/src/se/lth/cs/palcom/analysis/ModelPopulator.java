package se.lth.cs.palcom.analysis;

import java.io.IOException;
import java.io.PrintWriter;

import ist.palcom.resource.descriptor.DeviceID;
import se.lth.cs.palcom.analysis.gen.List;
import se.lth.cs.palcom.analysis.gen.PalcomSystem;
import se.lth.cs.palcom.analysis.gen.Root;
import se.lth.cs.palcom.analysis.gen.StaticInfo;
import se.lth.cs.palcom.communication.DefaultCommunicationManager;
import se.lth.cs.palcom.device.AbstractDevice;
import se.lth.cs.palcom.discovery.DeviceListener;
import se.lth.cs.palcom.discovery.ServiceBasedDiscoveryManager;
import se.lth.cs.palcom.logging.Logger;

public class ModelPopulator {

	public static void main(String[] args) throws IOException, InterruptedException {

		TmpDevice td = new TmpDevice(new DeviceID("X:test.analysis"), "Test analysis", "1");
		Logger.setLevel(Logger.LEVEL_DEBUG);
		td.start();
		Root tree = new Root(new PalcomSystem(new List<>()), new StaticInfo(new List<>(), new List<>()));

		DeviceListener dl = (new ModelDeviceListener(tree, td.getResourceManager()));

		td.getDiscoveryManager().addDeviceListener(dl);

		Thread.sleep(30000);

		synchronized (tree) {
			PrintWriter pw = new PrintWriter(System.out);
			tree.print(pw);
			tree.genDOT(pw);
			pw.flush();

		}
		System.exit(0);

	}

	

	private static class TmpDevice extends AbstractDevice {
		public TmpDevice(DeviceID id, String name, String version) {
			super(id, name, version, new DefaultCommunicationManager(id, true), false,
					ServiceBasedDiscoveryManager.INTEREST_FULL);

		}
	}

}

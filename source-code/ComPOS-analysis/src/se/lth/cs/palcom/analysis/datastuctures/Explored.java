package se.lth.cs.palcom.analysis.datastuctures;

public class Explored<T> {
	@Override
	public String toString() {
		return "Explored [base=" + base + ", explored=" + explored + "]";
	}

	private final T base;
	private final boolean explored;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((base == null) ? 0 : base.hashCode());
		result = prime * result + (explored ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Explored other = (Explored) obj;
		if (base == null) {
			if (other.base != null)
				return false;
		} else if (!base.equals(other.base))
			return false;
		if (explored != other.explored)
			return false;
		return true;
	}

	public T getBase() {
		return base;
	}

	public boolean isExplored() {
		return explored;
	}

	public Explored(T base, boolean explored) {
		super();
		this.base = base;
		this.explored = explored;
	}

}

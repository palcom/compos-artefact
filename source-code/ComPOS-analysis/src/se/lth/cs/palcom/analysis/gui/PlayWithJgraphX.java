//package se.lth.cs.palcom.analysis.gui;
//
//import java.util.Random;
//
//import javax.swing.JPanel;
//
//import com.mxgraph.layout.mxFastOrganicLayout;
//import com.mxgraph.layout.mxOrganicLayout;
//import com.mxgraph.layout.hierarchical.mxHierarchicalLayout;
//import com.mxgraph.swing.mxGraphComponent;
//import com.mxgraph.view.mxGraph;
//
//public class PlayWithJgraphX extends JPanel {
//
//	public PlayWithJgraphX() {
//
//		mxGraph graph = new mxGraph();
//		Object parent = graph.getDefaultParent();
//
//		graph.setAutoSizeCells(true);
//
//		graph.getModel().beginUpdate();
//		try {
//			
//			Random rand = new Random();
//			int nbr = 3;
//			Object[] vert = new Object[nbr];
//			for (int i = 0; i < vert.length; i++) {
//				vert[i] = graph.insertVertex(parent, null, "n" + i, 0, 0, 80, 20, "fillColor=green"); 
//				graph.insertVertex(vert[i], null, "c" + i, 0, 0, 80, 20, "fillColor=blue"); 
//				
//			}
//			
//			
//			for (int i = 0; i < nbr; i++) {
//				int index1 = rand.nextInt(nbr);
//				int index2 = ((rand.nextInt(nbr-2) + 1 + index1)) % nbr;
//				graph.insertEdge(parent, null, null, vert[index1], vert[index2]);
//			}
//		} finally {
//			graph.getModel().endUpdate();
//		}
//
//		new mxHierarchicalLayout(graph).execute(graph.getDefaultParent());
//	//	mxFastOrganicLayout layout = new mxFastOrganicLayout(graph);
//		
//	//	layout.setForceConstant(100);
//		
//	//  layout.execute(graph.getDefaultParent());
//
//
//		mxGraphComponent graphComponent = new mxGraphComponent(graph);
//		graphComponent.setEnabled(false);
//
//		add(graphComponent);
//
//	}
//
//}

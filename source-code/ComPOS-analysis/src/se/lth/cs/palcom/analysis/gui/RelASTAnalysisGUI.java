package se.lth.cs.palcom.analysis.gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintWriter;
import java.io.StringWriter;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import se.lth.cs.palcom.analysis.ModelDeviceListener;
import se.lth.cs.palcom.analysis.gen.List;
import se.lth.cs.palcom.analysis.gen.PalcomSystem;
import se.lth.cs.palcom.analysis.gen.Root;
import se.lth.cs.palcom.analysis.gen.StaticInfo;
import se.lth.cs.palcom.device.AbstractDevice;

public class RelASTAnalysisGUI extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3022987217718598713L;
	private Root tree;
	private JTextArea jf;
	

	public RelASTAnalysisGUI(AbstractDevice device) {
		tree = new Root(new PalcomSystem(new List<>()), new StaticInfo(new List<>(), new List<>()));
		device.getDiscoveryManager().addDeviceListener(new ModelDeviceListener(tree,device.getResourceManager()));
		
		setLayout(new BorderLayout());
		add(new GenerateButton(),BorderLayout.PAGE_END);
		
		
		
		jf = new JTextArea();
		add(new JScrollPane(jf),BorderLayout.CENTER);
		
		add(new DDAPanel(device, tree, jf), BorderLayout.PAGE_START);
//		add(new PlayWithJgraphX(), BorderLayout.LINE_START);
		
		
		
	}
	
	private class GenerateButton extends JButton implements ActionListener{

		/**
		 * 
		 */
		private static final long serialVersionUID = 8634555450398249538L;

		public GenerateButton() {
			super("Show overview");
			addActionListener(this);
			
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			String s = ""; 
			synchronized (tree) {
				StringWriter sv = new StringWriter();
				tree.genDOT(new PrintWriter(sv));
				sv.flush();
				s = sv.toString();
			}
			jf.setText(s);
		}
		
		
	}

}

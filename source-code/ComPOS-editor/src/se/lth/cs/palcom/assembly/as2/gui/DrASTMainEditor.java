package se.lth.cs.palcom.assembly.as2.gui;

import java.io.IOException;
import java.io.RandomAccessFile;

import org.jastadd.tree.edit.xml.JastAddXMLParser;

import internal.org.xmlpull.v1.XmlPullParserException;

public class DrASTMainEditor {
	public static Object DrAST_root_node;

	public static void main(String[] args) throws IOException {
		RandomAccessFile f;
		byte[] data = null;
		try {
			f = new RandomAccessFile(args[0], "r");
			data = new byte[(int) f.length()];
			f.read(data);
			try {
				DrAST_root_node =  JastAddXMLParser.parse(new String(data),
						"se.lth.cs.palcom.assembly.as2.gen.editor");
			} catch (XmlPullParserException e1) {
				throw new RuntimeException("XML Error 2", e1);
			} catch (IOException e1) {
				throw new RuntimeException("Io Error 2", e1);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

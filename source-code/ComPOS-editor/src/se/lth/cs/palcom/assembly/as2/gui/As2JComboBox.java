package se.lth.cs.palcom.assembly.as2.gui;
//
//import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.plaf.basic.BasicComboBoxUI;

//public class As2JComboBox extends JComboBox<String> {
//	
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 4L;
//
//	public As2JComboBox(String [] s) {
//		super(s);
//		setUI(BasicComboBoxUI.createUI(this));
//		setMaximumSize(getPreferredSize());
//		setBorder(BorderFactory.createEmptyBorder());
//		setFont(new Font("Monospaced", Font.PLAIN, 14));
//		setRenderer(new ListCellRenderer<String>() {
//
//			@Override
//			public Component getListCellRendererComponent(JList<? extends String> list, String value, int index,
//					boolean isSelected, boolean cellHasFocus) {
//				JLabel label = new JLabel(value);
//				if(isSelected){
//					label.setFont(new Font("Monospaced", Font.BOLD, 14));
//				} else {
//					label.setFont(new Font("Monospaced", Font.PLAIN, 14));
//				}
//				return label;
//			}
//			
//		});
//	}
//}
package se.lth.cs.palcom.assembly.as2.gui;

import org.jastadd.tree.edit.CompilerContainer;
import org.jastadd.tree.edit.SubclassExtractor;

import se.lth.cs.palcom.device.AbstractDevice;

public class As2CompilerContainer extends CompilerContainer {

	private AbstractDevice device;

	public As2CompilerContainer(SubclassExtractor se, AbstractDevice device) {
		super(se);
		this.device = device;
	}
	
	public AbstractDevice getDevice(){
		return device;
	}

}

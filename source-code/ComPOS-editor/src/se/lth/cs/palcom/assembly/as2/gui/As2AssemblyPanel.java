package se.lth.cs.palcom.assembly.as2.gui;

import java.awt.AWTEvent;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLayer;
import javax.swing.JLayeredPane;
import javax.swing.UIManager;
import javax.swing.plaf.LayerUI;
import javax.swing.text.AbstractWriter;

import org.jastadd.tree.edit.CompilerContainer;
import org.jastadd.tree.edit.SubclassExtractor;
import org.jastadd.tree.edit.custom.render.MainCustomContainer;
import org.jastadd.tree.edit.jtree.MainContainer;
import org.jastadd.tree.edit.jtree.MainJtreeContatiner;
import org.jastadd.tree.edit.xml.JastAddXMLParser;
import org.jastadd.tree.edit.xml.JastAddXMLSerializer;

import internal.org.xmlpull.v1.XmlPullParserException;
import naming.TypeVersion;
import naming.TypeVersionReference;
import naming.assembly.AssemblyTypeVersionName;
import naming.assembly.AssemblyTypeVersionReference;
import se.lth.cs.palcom.assembly.AssemblyContainer;
import se.lth.cs.palcom.assembly.as2.exception.As2Exception;
import se.lth.cs.palcom.assembly.as2.gen.editor.As2Unit;
import se.lth.cs.palcom.browsergui.AbstractAssemblyPanel;
import se.lth.cs.palcom.browsergui.AssemblyTransferHandler;
import se.lth.cs.palcom.browsergui.SaveStatusListener;
import se.lth.cs.palcom.browsergui.PONTransferableFactory;
import se.lth.cs.palcom.browsergui.dnd.MultiTransferable;
import se.lth.cs.palcom.browsergui.dnd.ResourceTransferable;
import se.lth.cs.palcom.device.AbstractDevice;

public class As2AssemblyPanel extends AbstractAssemblyPanel {

	List<SaveStatusListener> listners = new ArrayList<SaveStatusListener>();

	private boolean unsaved;

	private String fileName;

	private MainContainer gui;

	private CompilerContainer cc;

	private TypeVersionReference typeVersionRef;

	/**
	 * 
	 */
	private static final long serialVersionUID = 7581160474377128401L;

	public As2AssemblyPanel(String fileName, final AssemblyContainer ac, AssemblyTransferHandler ath) {
		this.fileName = fileName;
		setLayout(new BorderLayout());

		SubclassExtractor se = new SubclassExtractor("../Assembly-as2-xml-editor/dir.txt");
		this.cc = new As2CompilerContainer(se, ac.getDevice());

		ath.addDragStatusListner(new AssemblyTransferHandler.ATDragStatus() {
			@Override
			public void endedDrag() {
				As2AssemblyPanel.this.cc.stopedDrag();

			}

			@Override
			public void begunDrag(Object o) {
				if (o instanceof PONTransferableFactory) {
					Transferable browserTreeNode = ((PONTransferableFactory) o).createTransferable();
					if (browserTreeNode instanceof MultiTransferable) {
						MultiTransferable bTN = (MultiTransferable) browserTreeNode;
						As2AssemblyPanel.this.cc.startedDrag(bTN.getPONType());
					} else if (browserTreeNode instanceof ResourceTransferable<?>) {
						ResourceTransferable<?> bTN = (ResourceTransferable<?>) browserTreeNode;
						As2AssemblyPanel.this.cc.startedDrag(bTN.getResource());
					}
				}

			}
		});

		cc.addObserver(new Observer() {
			@Override
			public void update(Observable o, Object arg) {
				setUnsaved(true);
			}
		});
		this.gui = new MainCustomContainer(ac.getData(), cc, "se.lth.cs.palcom.assembly.as2.gen.editor");
		add(gui, BorderLayout.CENTER);
	}

	public As2AssemblyPanel(TypeVersion version, AbstractDevice device, AssemblyTransferHandler ath) {
		this.fileName = version.getTypeVersionReference().getTypeID().getReadableName();
		this.typeVersionRef = version.getTypeVersionReference();
		setLayout(new BorderLayout());

		SubclassExtractor se = new SubclassExtractor("../Assembly-as2-xml-editor/dir.txt");
		this.cc = new As2CompilerContainer(se, device);

		ath.addDragStatusListner(new AssemblyTransferHandler.ATDragStatus() {
			@Override
			public void endedDrag() {
				As2AssemblyPanel.this.cc.stopedDrag();

			}

			@Override
			public void begunDrag(Object o) {
				if (o instanceof PONTransferableFactory) {
					Transferable browserTreeNode = ((PONTransferableFactory) o).createTransferable();
					if (browserTreeNode instanceof MultiTransferable) {
						MultiTransferable bTN = (MultiTransferable) browserTreeNode;
						As2AssemblyPanel.this.cc.startedDrag(bTN.getPONType());
					} else if (browserTreeNode instanceof ResourceTransferable<?>) {
						ResourceTransferable<?> bTN = (ResourceTransferable<?>) browserTreeNode;
						As2AssemblyPanel.this.cc.startedDrag(bTN.getResource());
					}
				}

			}
		});

		cc.addObserver(new Observer() {
			@Override
			public void update(Observable o, Object arg) {
				setUnsaved(true);
			}
		});
		this.gui = new MainCustomContainer(version.getContent(), cc, "se.lth.cs.palcom.assembly.as2.gen.editor");

		if (typeVersionRef.getVersion().hasBasePart()) {
			add(gui, BorderLayout.CENTER);
		} else { // Makes it non -ediable
			JLayer<MainContainer> gui2 = new JLayer<MainContainer>(gui, new LayerUI<MainContainer>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				public void paint(Graphics g, JComponent c) {
					// paint the layer as is
					super.paint(g, c);
					// fill it with the translucent green
					g.setColor(new Color(128, 128, 128, 128));
					g.fillRect(0, 0, c.getWidth(), c.getHeight());
					g.setColor(new Color(228, 0, 0, 128));
					g.drawString("To edit, create new development version in the right panel", c.getWidth()/2, c.getHeight()/2);
					
				}

				public void installUI(JComponent c) {
					super.installUI(c);
					((JLayer) c).setLayerEventMask(AWTEvent.MOUSE_EVENT_MASK | AWTEvent.KEY_EVENT_MASK);
				}

				public void uninstallUI(JComponent c) {
					super.uninstallUI(c);
					((JLayer) c).setLayerEventMask(0);
				}

				public void eventDispatched(AWTEvent e, JLayer<? extends MainContainer> l) {
					if (e instanceof InputEvent) {
						InputEvent event = (InputEvent) e;
						event.consume();
					}
				}

			});
			add(gui2, BorderLayout.CENTER);
		}

	}

	@Override
	public byte[] getByteData() {
		return gui.getData();
	}

	@Override
	public void removeSaveStatusListener(SaveStatusListener ssl) {
		listners.remove(ssl);
	}

	@Override
	public void addSaveStatusListener(SaveStatusListener ssl) {
		listners.add(ssl);
	}

	@Override
	public void setUnsaved(boolean unsaved) {
		this.unsaved = unsaved;
		for (SaveStatusListener s : listners) {
			s.saveStatusChanged(!unsaved, this);
		}
	}

	@Override
	public boolean isUnsaved() {
		return unsaved;
	}

	@Override
	public String getFileName() {
		return fileName;
	}

	public static String createFile(String filename, AbstractDevice device) {
		SubclassExtractor se = new SubclassExtractor("../Assembly-as2-xml-editor/dir.txt");

		As2Unit as2 = new As2Unit();
		se.inferAll(as2);
		as2.setFormatVersion("0.1");
		// as2.getAssemblyBindings().getNameDef().setname(filename);

		as2.getAs2Assembly().createBaseVersion(new As2CompilerContainer(se, device), filename);

		try {
			return JastAddXMLSerializer.serializ(as2);
		} catch (IOException e) {
			throw new As2Exception(e);
		}
	}

	public static String createFile(AssemblyTypeVersionName atvn) {
		SubclassExtractor se = new SubclassExtractor("../Assembly-as2-xml-editor/dir.txt");

		As2Unit as2 = new As2Unit();
		se.inferAll(as2);
		as2.setFormatVersion("0.1");
		// as2.getAssemblyBindings().getNameDef().setname(filename);

		as2.getAs2Assembly().setVersion(atvn);

		try {
			return JastAddXMLSerializer.serializ(as2);
		} catch (IOException e) {
			throw new As2Exception(e);
		}
	}

	public static TypeVersion createNewFrom(AssemblyTypeVersionName newVersion, TypeVersion oldAssembly) {
		try {
			As2Unit as2 = (As2Unit) JastAddXMLParser.parse(new String(oldAssembly.getContent()),
					"se.lth.cs.palcom.assembly.as2.gen.editor");
			as2.getAs2Assembly().setVersion(newVersion);
			return new TypeVersion(newVersion.getReference(), JastAddXMLSerializer.serializ(as2).getBytes());
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;

	}

	@Override
	public TypeVersion createTypeVersion() {
		return new TypeVersion(typeVersionRef, getByteData());
	}

}

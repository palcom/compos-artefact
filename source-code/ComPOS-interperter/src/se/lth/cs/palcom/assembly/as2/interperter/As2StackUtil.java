package se.lth.cs.palcom.assembly.as2.interperter;

import ist.palcom.resource.descriptor.Command;

public class As2StackUtil {

	public static void setPopTheStack(Command command) {
		int sequenceNumber = -1;
		int sourceConnection = -1;
		int sendId = -1;
		int reactionId = -1;
		int localSelector = -1;
		if (command.getReplyStack().length != 0 && command.peekReplyStack() == -1) {
			command.popReplyStack();
			sequenceNumber = command.popReplyStack();
			sourceConnection = command.popReplyStack();
			sendId = command.popReplyStack();
			reactionId = command.popReplyStack();
		}
		command.setSeqNbr(sequenceNumber);
		command.setSource(sourceConnection);
		command.setSendId(sendId);
		command.setReactionId(reactionId);
	}
	
	public static void pushTheStack(Command command) {
		if (command.getSeqNbt() != -1) {
			command.pushReplyStack(command.getReactionId());
			command.pushReplyStack(command.getSendId());
			command.pushReplyStack(command.getSource());
			command.pushReplyStack(command.getSeqNbt());
			command.pushReplyStack(-1);
		}
	}

	public static void setPopTheStackWithSelectorOnTop(Command command) {
		int localSel = command.popReplyStack();
		setPopTheStack(command);
		command.pushReplyStack(localSel);
	}

	public static void pushTheStackWithOne(Command com) {
		int localSel = com.popReplyStack();
		pushTheStack(com);
		com.pushReplyStack(localSel);
	}
	
}

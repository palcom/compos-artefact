package se.lth.cs.palcom.assembly.as2.interperter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import ist.palcom.resource.descriptor.Command;
import se.lth.cs.palcom.assembly.as2.gen.interpreter.Action;
import se.lth.cs.palcom.assembly.as2.util.As2ConnInstId;
import se.lth.cs.palcom.command.trace.CommandTracer;
import se.lth.cs.palcom.communication.connection.DirectedConnection;

public abstract class  ActivationRecord {
	
	public static int globalID = 0; // May be persistant later
	
	protected Action pc;
	private ArrayList<ActivationRecord> childRecords = new ArrayList<>();
	

	protected CommandTracer ct;

	
	public ActivationRecord(Action pc2, CommandTracer ct2) {
		this.ct = ct2;
		this.pc = pc2;
	}
	

	abstract public void addVar(String name,byte[] data);
	
	abstract public byte[] getVar(String name);
	
	abstract public void performUntilWait(DirectedConnection connection, Command command);

	
	
	public final Action getPc() {
		return pc;
	}
	public final void setPc(Action pc) {
		this.pc = pc;
	}

	abstract public int getId();
	
	public boolean checkId(int reciveId, int offset) {
		return (getId() + offset) == reciveId;
		
		
	}
	

//	public final int nextSession() {
//		id = globalID;
//		globalID += 1;
//		return id;
	// }

	
 	
	abstract public void resetMatch();
	
	abstract public void setMatch();
	
	abstract public boolean matched();
		
	
	public final int numChildRecords(){
		return childRecords.size();
	}
	
	public final ActivationRecord getChildRecord(int i){
		return childRecords.get(i);
	}
	
	public final void clearChildRecords() {
		childRecords.clear();
	}
	
	public final void clearAllChildRecords() {
		clearChildRecords();
	}
	
	
	public final void addChildRecord(Action action){
			ChildActivationRecord toAdd = new ChildActivationRecord(action,ct, this);
			childRecords.add(toAdd);
	}

	public CommandTracer getCommandTracer() {
	  return ct;	
	}


	public abstract int getReactionId();	
	
	


	public CommandTracer getCt() {
		return ct;
	}


	abstract public As2ConnInstId getAs2ConnInstId();
		
	
	
}
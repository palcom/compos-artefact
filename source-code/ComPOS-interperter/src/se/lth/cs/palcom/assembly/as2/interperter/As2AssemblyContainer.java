package se.lth.cs.palcom.assembly.as2.interperter;

import java.io.IOException;
import java.util.ArrayList;

import org.jastadd.tree.edit.xml.JastAddXMLParser;

import internal.org.xmlpull.v1.XmlPullParserException;
import ist.palcom.resource.descriptor.Command;
import ist.palcom.resource.descriptor.ListAddress;
import ist.palcom.resource.descriptor.PRDAssemblyVer;
import ist.palcom.resource.descriptor.PRDData;
import ist.palcom.resource.descriptor.ServiceID;
import naming.NamingConverter;
import naming.assembly.AssemblyTypeVersionName;
import naming.assembly.AssemblyTypeVersionRole;
import naming.service.ServiceInstanceReference;
import naming.service.ServiceTypeID;
import naming.service.ServiceTypeVersionReference;
import se.lth.cs.palcom.assembly.AssemblyContainer;
import se.lth.cs.palcom.assembly.AssemblyInterface;
import se.lth.cs.palcom.assembly.FireEvent;
import se.lth.cs.palcom.assembly.RuntimeContext;
import se.lth.cs.palcom.assembly.as2.exception.As2Exception;
import se.lth.cs.palcom.assembly.as2.exception.As2ParseException;
import se.lth.cs.palcom.assembly.as2.gen.interpreter.Action;
import se.lth.cs.palcom.assembly.as2.gen.interpreter.As2Unit;
import se.lth.cs.palcom.assembly.as2.gen.interpreter.ServiceVariable;
import se.lth.cs.palcom.assembly.as2.gen.interpreter.SynthesizedService;
import se.lth.cs.palcom.assembly.as2.gen.interpreter.WhenDo;
import se.lth.cs.palcom.assembly.as2.util.As2ConnInstId;
import se.lth.cs.palcom.common.collections.Collection;
import se.lth.cs.palcom.common.collections.HashMap;
import se.lth.cs.palcom.common.collections.Iterator;
import se.lth.cs.palcom.common.collections.Map;
import se.lth.cs.palcom.device.AbstractDevice;
import se.lth.cs.palcom.discovery.ResourceException;
import se.lth.cs.palcom.logging.Logger;
import se.lth.cs.palcom.service.mirror.MirrorRequesterService;

public class As2AssemblyContainer extends AssemblyContainer implements AssemblyInterface {

	private static int nameCount = 0;

	private byte[] data;
	private RuntimeContext runtimeContext;
	private ListAddress addr;
	private String instanceName = "1";

	private As2Unit as2Assembly;
	private ArrayList<As2MirrorService> mirrorList = new ArrayList<>();
	private ArrayList<As2SynthService> synthList = new ArrayList<>();
	// private ActivationRecord activationRecord = null;

	private HashMap connIndt = new HashMap();

	private As2MirrorRequesterService mirrorRequesterService;

	public As2AssemblyContainer(FireEvent fireEvent, AbstractDevice currentDevice, byte[] data,
			RuntimeContext runtimeContext) {
		super(fireEvent, currentDevice);
		this.data = data;
		this.runtimeContext = runtimeContext;
		try {
			parse();
			this.mirrorRequesterService = new As2MirrorRequesterService(
					as2Assembly.getAs2Assembly().getAssemblyTypeVersionName().getReference(), instanceName,
					currentDevice);
		} catch (As2ParseException e) {
			name = "as2UnKown" + nameCount++;
			Logger.log("Can not parse assembly" + e.getMessage(), Logger.CMP_ASSEMBLY, Logger.LEVEL_DEBUG);
		}
		// as2Assembly.setRuntimeContext(runtimeContext);
	}

	public As2AssemblyContainer(FireEvent fireEvent, AbstractDevice currentDevice, byte[] data, String instanceName,
			RuntimeContext runtimeContext) {
		this(fireEvent, currentDevice, data, runtimeContext);
		this.instanceName = instanceName;

	}

	@Override
	public String getSuffix() {
		return As2AssemblyLoader.AS2_SUFIX;
	}

	@Override
	public byte[] getData() {
		return data;
	}

	@Override
	public void start() {
		if (isEnabled()) {
			return;
		}
		mirrorRequesterService.register();
		setEnabled(true);
		initMirrorserivices();
		initSynthservice();
		fireAssemblyStart();
		register();
	}

	private void initSynthservice() {
		for (SynthesizedService ps : as2Assembly.getAs2Assembly().getSynthesizedServices()
				.getSynthesizedServiceList()) {
			AssemblyTypeVersionName atvn = as2Assembly.getAs2Assembly().getAssemblyTypeVersionName();
			ServiceTypeID stid;
			stid = new ServiceTypeID(ps.getNameDef().getName(), atvn.getAssemblyTypeID(), ps.getNameDef().getName());
			ServiceTypeVersionReference stvr = new ServiceTypeVersionReference(stid, atvn.getVersion());
			ServiceID sid = NamingConverter.makeOldServiceID(stvr);
			String helpText = ps.hasHelpText() ? ps.getHelpText().getText() : "";
			As2SynthService as2SyntService = new As2SynthService(getDevice(), sid, stid.getReadableName(), instanceName,
					helpText, ps, this);
			as2SyntService.addInCommands(ps.allInCommandDefs());
			as2SyntService.addOutCommands(ps.allOutCommandDefs());
			as2SyntService.register();
			as2SyntService.start();
			synthList.add(as2SyntService);
		}

	}

	private void initMirrorserivices() {
		Logger.log(
				"%AS2 init mirror service with nbr mirrors: "
						+ as2Assembly.getAs2Assembly().getBindings().getServiceVariables().getNumChild(),
				Logger.CMP_ASSEMBLY, Logger.LEVEL_DEBUG);
		for (ServiceVariable sd : as2Assembly.getAs2Assembly().getBindings().getServiceVariables()) {
			ServiceInstanceReference siid = sd.getServiceInstanceReference(device);
			As2MirrorService ms = new As2MirrorService(sd, this);
			// setAliasListener(sd, ms);
			// setServiceListener(sd, ms);
			// setDeviceListener(sd, ms);
			mirrorList.add(ms);
			Logger.log(
					"%AS2 added mirror service to: " + sd.getNameDef().getName() + " = " + ms.getServiceName()
							+ " in list of size: " + mirrorList.size() + " on device "
							+ siid.getDIR().getDeviceInstanceID().getUniqueName(),
					Logger.CMP_ASSEMBLY, Logger.LEVEL_DEBUG);
			try {
				device.getDiscoveryManager().getMirrorService(NamingConverter.makeSIID(siid),
						mirrorRequesterService.getLocalAddress(), ms);

			} catch (ResourceException e) {
				throw new As2Exception("Can not create mirrorservice!", e);
			}
		}
	}

	// private void setDeviceListener(ServiceDef sd, As2MirrorService ms) {
	// if (runtimeContext instanceof DeviceConfigRuntimeContext) {
	// DeviceConfigRuntimeContext dcrc = (DeviceConfigRuntimeContext)
	// runtimeContext;
	// dcrc.getParameterManager().addParameterListener(sd.getDeviceParam().getname(),
	// new ParameterListener() {
	// @Override
	// public void parameterChanged(String parameter) {
	// Object sir = dcrc.getPONParameter(sd.getServiceDef().getname(),
	// ServiceInstanceReference.class);
	// if (sir == null) {
	// reconnect(ms);
	// }
	// }
	// });
	// }
	//
	// }
	//
	// private void setServiceListener(ServiceDef sd, As2MirrorService ms) {
	// if (runtimeContext instanceof DeviceConfigRuntimeContext) {
	// DeviceConfigRuntimeContext dcrc = (DeviceConfigRuntimeContext)
	// runtimeContext;
	// dcrc.getParameterManager().addParameterListener(sd.getServiceDef().getname(),
	// new ParameterListener() {
	// @Override
	// public void parameterChanged(String parameter) {
	// reconnect(ms);
	// }
	// });
	// }
	// }
	//
	// private void setAliasListener(ServiceDef serviceDef, As2MirrorService ms)
	// {
	// if (serviceDef.hasAlias()) {
	// ms.setAlias(new String(serviceDef.getAlias().getValue(null)));
	// ParamValue po = serviceDef.getAlias().getParamObserver();
	// if (po != null) {
	// po.addObserver(new Observer() {
	// @Override
	// public void update(Observable o, Object arg) {
	// ms.setAlias(po.getParamValue());
	// reconnect(ms);
	// }
	// });
	// } else if (serviceDef.getAlias() instanceof NameUseArg
	// && runtimeContext instanceof DeviceConfigRuntimeContext) {
	// DeviceConfigRuntimeContext dcrc = (DeviceConfigRuntimeContext)
	// runtimeContext;
	// NameUseArg nua = (NameUseArg) serviceDef.getAlias();
	// dcrc.getParameterManager().addParameterListener(nua.getNameUse().getname(),
	// new ParameterListener() {
	// @Override
	// public void parameterChanged(String parameter) {
	// ms.setAlias(new String(serviceDef.getAlias().getValue(null)));
	// reconnect(ms);
	// }
	// });
	// }
	// }
	// }

	@Override
	public void stop() {
		if (!isEnabled()) {
			return;
		}
		setEnabled(false);
		stopMirrorserivices();
		stopSynthSerivices();
		fireAssemblyStop();
		unRegister();

	}

	private void stopMirrorserivices() {
		for (As2MirrorService ms : mirrorList) {
			ms.close();
			ms.deRefAST();
		}
		mirrorList.clear();

	}

	private void stopSynthSerivices() {
		for (As2SynthService ms : synthList) {
			ms.deRefAST();
			ms.unregister();
			ms.stop();
		}
		synthList.clear();

	}

	@Override
	public void update(byte[] content, boolean enabled) {
		if (content != null) {
			data = content;
			parse();
		}
		stopAndMaybeStart(enabled);
	}

	private void stopAndMaybeStart(boolean enabled) {
		if (isEnabled()) {
			stop();
		}
		if (enabled) {
			start();
		}
	}

	@Override
	public void reload() {
		stopAndMaybeStart(isEnabled());
	}

	private void parseWithE() throws IOException, XmlPullParserException {
		as2Assembly = (As2Unit) JastAddXMLParser.parse(new String(data),
				"se.lth.cs.palcom.assembly.as2.gen.interpreter");
		name = as2Assembly.getAs2Assembly().getAssemblyTypeVersionName().getAssemblyTypeID().getReadableName();
	}

	private void parse() {
		try {
			parseWithE();
		} catch (IOException e) {
			throw new As2ParseException(e);
		} catch (XmlPullParserException e) {
			throw new As2ParseException(e);
		} catch (As2Exception e) {
			throw new As2ParseException("Error in file " + new String(data), e);
		}
	}

	private void register() {

		addr = device.getAnnouncementManager().registerAssembly(this, null, name);
		device.getAnnouncementManager().registerStatus(addr, PRDData.FULLY_OPERATIONAL);
	}

	private void unRegister() {
		device.getAnnouncementManager().unregisterService(addr);
	}

	@Override
	public PRDAssemblyVer getAssemblyVer() {
		throw new RuntimeException("Can not be implemented shall be removed");
	}

	@Override
	public AssemblyTypeVersionRole getAssemblyTypeVersionRole() {
		AssemblyTypeVersionRole atvr = new AssemblyTypeVersionRole(
				as2Assembly.getAs2Assembly().getAssemblyTypeVersionName().getReference(), instanceName);
		atvr.setEnabled(isEnabled());
		return atvr;
	}

	@Override
	public Map getMirros() {
		Map mirrors = new HashMap();
		for (As2MirrorService ms : mirrorList) {
			mirrors.put(ms.getServiceName(), ms);
		}
		return mirrors;
	}

	public Collection getActivationRecords() {
		return connIndt.values();
	}

	public void removeActivationForConnection(int id) {
		Iterator itr = connIndt.entrySet().iterator();
		ArrayList<As2ConnInstId> al = new ArrayList<>();
		while (itr.hasNext()) {
			Map.Entry entry = (Map.Entry) itr.next();
			As2ConnInstId acii = (As2ConnInstId) entry.getKey();
			if (acii.getLocalSelector() == id) {
				al.add(acii);
			}
		}
		for (As2ConnInstId acii : al) {
			connIndt.remove(acii);
		}
	}

	public ActivationRecord newActivationRecord(As2ConnInstId connIntsId, WhenDo wd) {
		removeOldActivations(connIntsId);
		ActivationRecord ar = new RootActivationRecord(wd, getCommandTracer(), connIntsId);
		getCommandTracer().trace("New record");
		connIndt.put(connIntsId, ar);
		return ar;
	}

	private void removeOldActivations(As2ConnInstId connIntsId) {
		Iterator itr = connIndt.entrySet().iterator();
		ArrayList<As2ConnInstId> al = new ArrayList<>();
		while (itr.hasNext()) {
			Map.Entry entry = (Map.Entry) itr.next();
			As2ConnInstId acii = (As2ConnInstId) entry.getKey();
			if (connIntsId.abots(acii)) {
				al.add(acii);
			}
		}
		for (As2ConnInstId acii : al) {
			connIndt.remove(acii);
		}
	}

	public void newActivationRecord(int localSelector, Command command, WhenDo whenDo) {
		if (command.getSource() < 0) {
			int max = findMaxSource(localSelector);
			command.setSource(localSelector);
			command.setSeqNbr(max + 1);
		}
		newActivationRecord(new As2ConnInstId(localSelector, command), whenDo);
	}

	private int findMaxSource(int localSelector) {
		Iterator itr = connIndt.entrySet().iterator();
		int max = 1;
		while (itr.hasNext()) {
			Map.Entry entry = (Map.Entry) itr.next();
			As2ConnInstId acii = (As2ConnInstId) entry.getKey();
			if (acii.getSource() == localSelector && acii.getSeqNbr() > max) {
				max = acii.getSeqNbr();
			}
		}
		return max;
	}

	// public void reconnect(As2MirrorService as2MirrorService) {
	// as2MirrorService.close();
	// ServiceInstanceReference siid =
	// as2MirrorService.getServiceDef().getServiceInstanceReference(runtimeContext);
	// System.out.println("Reconnect mirror: " + siid.getDIR());
	// try {
	// device.getDiscoveryManager().getMirrorService(siid, as2MirrorService);
	// } catch (ResourceException e) {
	// throw new As2Exception("Can not create mirrorservice!", e);
	// }
	//
	// }
}

package se.lth.cs.palcom.assembly.as2.interperter;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import ist.palcom.resource.descriptor.Command;
import ist.palcom.resource.descriptor.Param;
import se.lth.cs.palcom.assembly.as2.gen.interpreter.Action;
import se.lth.cs.palcom.assembly.as2.gen.interpreter.ServiceVariable;
import se.lth.cs.palcom.assembly.as2.gen.interpreter.WhenDo;
import se.lth.cs.palcom.assembly.as2.interperter.As2Executer.EventToExecute;
import se.lth.cs.palcom.assembly.as2.util.As2ConnInstId;
import se.lth.cs.palcom.command.trace.CommandTracer;
import se.lth.cs.palcom.common.collections.Iterator;
import se.lth.cs.palcom.communication.connection.DirectedConnection;
import se.lth.cs.palcom.communication.connection.DynamicConnection;
import se.lth.cs.palcom.communication.connection.Writable;
import se.lth.cs.palcom.discovery.Resource;
import se.lth.cs.palcom.service.mirror.CommandMirrorService;
import se.lth.cs.palcom.service.mirror.MirrorServiceListener;

public class As2MirrorService extends CommandMirrorService implements MirrorServiceListener {

	private ServiceVariable serviceVariable;
	private CommandTracer ct;
	private As2AssemblyContainer container;

	public As2MirrorService(ServiceVariable sd, As2AssemblyContainer container) {
		super(false);
		if(sd.hasAliasValue()) {
			setAlias(new String(sd.getAliasValue().getValue(null, null)));
		}
		enableAutoConnect();
		this.serviceVariable = sd;
		sd.setMirror(this);
		addListener(this);
		ct = container.getCommandTracer();
		this.container = container;
	}

	public boolean sameConnection(DirectedConnection con) {
		return con == getConnection();

	}

	public String getServiceName() {
		return serviceVariable.getNameDef().getName();
	}

	@Override
	public void invoke(Command command) {
	//	ct.trace(command, "in mirror");
		As2StackUtil.setPopTheStack(command);
		ArrayList<WhenDo> dependentConditions = serviceVariable.dependentConditions();
		performActions(command, dependentConditions);
		super.invoke(command);
	}


	public void performActions(final Command command, ArrayList<WhenDo> dependentConditions) {
	//	ct.traceReceiveCommand(command);
	//	ct.trace(command, "Incomming to mirror:" + serviceVariable.getNameDef().getName());
		final DirectedConnection dc = (DirectedConnection) getConnection();
		for (final WhenDo whenDo : dependentConditions) {
			if (whenDo.match(command)) {
				As2Executer.addEventToExecute(new EventToExecute() {
					@Override
					public void execute() {
						command.pushReplyStack(dc.getLocalSelector());
						ct.trace(command, "match");
						container.newActivationRecord(dc.getLocalSelector(), command, whenDo);
					}
				});
			}
		}
		execute(command);
	}

	public void execute(final Command command) {
		//ct.trace(command, "Add event for");
		As2Executer.addEventToExecute(new EventToExecute() {
			@Override
			public void execute() {
				//ct.trace(command,"execute mirror event");
				Iterator itr = container.getActivationRecords().iterator();
				while (itr.hasNext()) {
					ActivationRecord ac = (ActivationRecord) itr.next();
					//ct.trace("exe");
					ac.performUntilWait((DirectedConnection) getConnection(), command);
				}
			}
		});
	}

	public void performActions(ArrayList<WhenDo> dependentConditions) {
		final DirectedConnection dc = (DirectedConnection) getConnection();
		for (final WhenDo whenDo : dependentConditions) {
			As2Executer.addEventToExecute(new EventToExecute() {
				@Override
				public void execute() {
					container.newActivationRecord(new As2ConnInstId(dc.getLocalSelector(), new int[0],-1,-1,-1,-1), whenDo)
							.performUntilWait(dc, null);
				}
			});
		}
	}

	public Command findOutCommand(String s) {
		return findCommand(s, Command.DIRECTION_IN);
	}

	public void deRefAST() {
		serviceVariable.setMirror(null);

	}

	public ServiceVariable getServiceDef() {
		return serviceVariable;
	}

	@Override
	public void connectionUpdated(boolean connected, String message) {
		if (connected) {
			ArrayList<WhenDo> dependentConditions = serviceVariable.dependentConnectedConditions();
			performActions(dependentConditions);
		} else {
			ArrayList<WhenDo> dependentConditions = serviceVariable.dependentDisconnectedConditions();
			performActions(dependentConditions);
		}

	}

	@Override
	public void statusUpdated(byte status, String description) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean send(Command command) {
		As2StackUtil.pushTheStack(command);
		ct.traceSendCommand(command);
		return super.send(command);
	}


	@Override
	public boolean send(Command req, Command command) {
		As2StackUtil.pushTheStack(command);
		ct.traceSendCommand(command);
		return super.send(req, command);
	}

	@Override
	public boolean send(Command command, int[] replyStack) {
		throw new RuntimeException("Do not call");
		
	//	As2StackUtil.pushTheStack(command);
	//	ct.traceSendCommand(command);
	//	return super.send(command, replyStack);
	}
	
	@Override
	public Command findCommand(String id, String direction) {
		//ct.trace(id);
		Command res = super.findCommand(id, direction);
		//ct.trace( "is null: " + (res == null));
		
		return res;
	}
	
	@Override
	public void available(Resource resource) {
		//ct.trace(getServiceName() + " : " + resource.getClass().getCanonicalName());
		super.available(resource);
	}

}

package se.lth.cs.palcom.assembly.as2.interperter;

import ist.palcom.resource.descriptor.Command;
import naming.assembly.AssemblyInstanceReference;
import naming.assembly.AssemblyTypeVersionName;
import naming.assembly.AssemblyTypeVersionReference;
import se.lth.cs.palcom.common.PalComVersion;
import se.lth.cs.palcom.communication.connection.Readable;
import se.lth.cs.palcom.device.AbstractDevice;
import se.lth.cs.palcom.discovery.DiscoveryManager;
import se.lth.cs.palcom.service.AbstractSimpleService;
import se.lth.cs.palcom.service.distribution.UnicastDistribution;
import se.lth.cs.palcom.service.mirror.MirrorRequesterService;

public class As2MirrorRequesterService extends AbstractSimpleService {

	
	public As2MirrorRequesterService(AssemblyTypeVersionReference assembly, String instance, AbstractDevice container) {
		super(container, assembly.converToOldService(),
				PalComVersion.DEFAULT_SERVICE_INTERACTION_PROTOCOL,
				assembly.getVersion().getReadableName(), assembly.getAssemblyTypeID().getReadableName(),
				instance,
				"AlfredUglyHack", new UnicastDistribution(true));
	}


	public void start() {
		// No-op. Will not need to be running, ever.
	}

	protected void invoked(Readable conn, Command command) {
		// Will never get invoked.
	}
}

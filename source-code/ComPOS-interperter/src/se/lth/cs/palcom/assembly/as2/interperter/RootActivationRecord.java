package se.lth.cs.palcom.assembly.as2.interperter;

import java.util.HashMap;

import ist.palcom.resource.descriptor.Command;
import se.lth.cs.palcom.assembly.as2.gen.interpreter.Action;
import se.lth.cs.palcom.assembly.as2.gen.interpreter.WhenDo;
import se.lth.cs.palcom.assembly.as2.util.As2ConnInstId;
import se.lth.cs.palcom.command.trace.CommandTracer;
import se.lth.cs.palcom.communication.connection.DirectedConnection;

public class RootActivationRecord extends ActivationRecord{
	
	
	private HashMap<String, byte[]> vars;
	private boolean match;
	private int id;
	private int reactionId;
	private As2ConnInstId connInstId; 
	
	public RootActivationRecord(WhenDo wd, CommandTracer ct, As2ConnInstId connInstId) {
		super(wd.firstAction(),ct);
		//System.out.println("%%Alfred set multi to: " + multiplexId);
		
		reactionId = globalID;
		globalID += 1;
		vars = new HashMap<>();
		id = globalID;
		this.pc = wd.firstAction(); 
		this.connInstId = connInstId;
		
	}
	
	public void addVar(String name,byte[] data){
		vars.put(name, data);
		ct.assign(name, data);
	}
	public byte[] getVar(String name){
		return vars.get(name);
	}
	
	public void performUntilWait(DirectedConnection connection, Command command) {
		do{
			//System.out.println("%Alfred Perform: " + getPc());
		}
		while(getPc().perform(this,connection, command));
		resetMatch();
	}
	public void resetMatch() {
		this.match = false;
	}
	
	public void setMatch() {
		this.match = true;
	}
	
	public boolean matched() {
		return match;
		
	}

	@Override
	public As2ConnInstId getAs2ConnInstId() {
		return connInstId;
	}


	@Override
	public int getId() {
		return id;
	}

	@Override
	public int getReactionId() {
		return reactionId;
	}

	@Override
	public String toString() {
		return "RootActivationRecord [vars=" + vars + ", match=" + match + ", id=" + id + ", reactionId=" + reactionId
				+ ", connInstId=" + connInstId + "]";
	}
	
	
}
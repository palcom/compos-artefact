package se.lth.cs.palcom.assembly.as2.util;

import java.util.Arrays;

import ist.palcom.resource.descriptor.Command;

public class As2ConnInstId {
	private final int localSelector;
	private final int[] replyStack;
	private final int seqNbr;
	private final int source;
	private final int sendId;
	private final int reactionId;

	public boolean abots(As2ConnInstId old) {
		if(getSeqNbr() > old.getSeqNbr() && getSource() == old.getSource()) {
			// If a new epoch has begun
			return true;
		}
		return false;
	}

	public As2ConnInstId(int localSelector, int[] replyStack, int seqNbr, int source, int sendId, int reactionId) {
		super();
		this.localSelector = localSelector;
		this.replyStack = replyStack;
		this.seqNbr = seqNbr;
		this.source = source;
		this.sendId = sendId;
		this.reactionId = reactionId;
	}

	public As2ConnInstId(int localSelector, Command c) {
		super();
		this.localSelector = localSelector;
		this.replyStack = c.getReplyStack();
		this.seqNbr = c.getSeqNbt();
		this.source = c.getSource();
		this.sendId = c.getSendId();
		this.reactionId = c.getReactionId();
	}

	public int getLocalSelector() {
		return localSelector;
	}

	public int[] getReplyStack() {
		return replyStack;
	}

	public int getSeqNbr() {
		return seqNbr;
	}

	public int getSource() {
		return source;
	}

	public int getSendId() {
		return sendId;
	}

	public int getReactionId() {
		return reactionId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + localSelector;
		result = prime * result + reactionId;
		result = prime * result + Arrays.hashCode(replyStack);
		result = prime * result + sendId;
		result = prime * result + seqNbr;
		result = prime * result + source;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		As2ConnInstId other = (As2ConnInstId) obj;
		if (localSelector != other.localSelector)
			return false;
		if (reactionId != other.reactionId)
			return false;
		if (!Arrays.equals(replyStack, other.replyStack))
			return false;
		if (sendId != other.sendId)
			return false;
		if (seqNbr != other.seqNbr)
			return false;
		if (source != other.source)
			return false;
		return true;
	}


}

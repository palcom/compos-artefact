package se.lth.cs.palcom.assembly.as2.interperter;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import se.lth.cs.palcom.assembly.as2.exception.As2Exception;


public class As2Executer {

	private static final BlockingQueue<EventToExecute> queue = new LinkedBlockingQueue<>();
	private static final MainExecuter me = new MainExecuter(); 
	
	public static void addEventToExecute(EventToExecute event) {
		if(me.getState() == Thread.State.NEW){
			me.start();
		}
		try {
			queue.put(event);
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new As2Exception(e);
		}
	}
	
	public static interface EventToExecute {
		public void execute();
	}
	
	private static class MainExecuter extends Thread {
		@Override
		public void run() {
			while (!isInterrupted()) {
				try {
					EventToExecute event = queue.take();
					event.execute();
				} catch (Exception e) {
					e.printStackTrace();

				}
			}
		}
	}
}

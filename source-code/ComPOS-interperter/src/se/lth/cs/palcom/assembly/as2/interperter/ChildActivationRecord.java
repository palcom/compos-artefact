package se.lth.cs.palcom.assembly.as2.interperter;

import ist.palcom.resource.descriptor.Command;
import se.lth.cs.palcom.assembly.as2.gen.interpreter.Action;
import se.lth.cs.palcom.assembly.as2.util.As2ConnInstId;
import se.lth.cs.palcom.command.trace.CommandTracer;
import se.lth.cs.palcom.communication.connection.DirectedConnection;

class ChildActivationRecord extends ActivationRecord {

	private ActivationRecord parent;

	ChildActivationRecord(Action pc,CommandTracer ct, ActivationRecord parent) {
		super(pc,ct);
		this.parent = parent; 
	}
	
	@Override
	public void resetMatch() {
		parent.resetMatch();
	}
	
	@Override
	public void setMatch() {
		parent.setMatch();
	}
	
	@Override
	public boolean matched() {
		return parent.matched();
	}
	
	@Override
	public void performUntilWait(DirectedConnection connection, Command command) {
		do{
			
		}
		while(getPc().perform(this,connection, command));
	}
	
	@Override
	public void addVar(String name, byte[] data) {
		parent.addVar(name, data);
	}
	
	@Override
	public byte[] getVar(String name) {
		return parent.getVar(name);
	}
	




	@Override
	public int getId() {
		return parent.getId();
	}

	@Override
	public String toString() {
		return "ChildActivationRecord [parent=" + parent + ", getId()=" + getId() + "]";
	}

	@Override
	public int getReactionId() {
		return parent.getReactionId();
	}

	@Override
	public As2ConnInstId getAs2ConnInstId() {
		return parent.getAs2ConnInstId();
	}

}

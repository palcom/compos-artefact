package se.lth.cs.palcom.assembly.as2.interperter;

import java.io.IOException;

import org.jastadd.tree.edit.xml.JastAddXMLParser;
import org.jastadd.tree.edit.xml.JastAddXMLSerializer;

import naming.assembly.AssemblyTypeVersionReference;
import naming.assembly.AssemblyTypeVersionRole;
import se.lth.cs.palcom.assembly.AssemblyContainer;
import se.lth.cs.palcom.assembly.AssemblyLoadException;
import se.lth.cs.palcom.assembly.AssemblyLoader;
import se.lth.cs.palcom.assembly.FireEvent;
import se.lth.cs.palcom.assembly.RuntimeContext;
import se.lth.cs.palcom.assembly.as2.gen.interpreter.As2Unit;
import se.lth.cs.palcom.device.AbstractDevice;

public class As2AssemblyLoader implements AssemblyLoader {

	public static final String AS2_SUFIX = ".as2";
	private AbstractDevice currentDevice;
	private FireEvent fireEvent;

	public As2AssemblyLoader(AbstractDevice currentDevice, FireEvent fireEvent) {
		this.currentDevice = currentDevice;
		this.fireEvent = fireEvent;
	}

	// public AssemblyContainer loadAssembly(byte[] data, boolean enabled,
	// RuntimeContext runtimeContext) {
	// As2AssemblyContainer res = new
	// As2AssemblyContainer(fireEvent,currentDevice, data,runtimeContext);
	// if(enabled){
	// res.start();
	// }
	// return res;
	// }

	public String getSuffix() {
		return AS2_SUFIX;
	}

	public As2AssemblyContainer loadAssembly(byte[] data, boolean enabled, RuntimeContext runtimeContext)
			throws AssemblyLoadException {
		As2AssemblyContainer res = new As2AssemblyContainer(fireEvent, currentDevice, data, runtimeContext);
		if (enabled) {
			res.start();
		}
		return res;
	}

	@Override
	public AssemblyContainer loadAssembly(AssemblyTypeVersionRole assemblyRole, byte[] data, boolean enabled,
			RuntimeContext runtimeContext) throws AssemblyLoadException {
		/*
		 * TODO: Alfred, please implement this method. Thanks! (\(^_^)/)
		 */
		As2AssemblyContainer res = new As2AssemblyContainer(fireEvent, currentDevice, data,
				assemblyRole.getRole().getUniqueName(), runtimeContext);
		if (enabled) {
			res.start();
		}
		return res;
		// return null;
	}

	@Override
	public AssemblyTypeVersionReference validateAssemblyFile(byte[] data) throws AssemblyLoadException {
		return validateAssemblyFileSatic(data);
	}

	public static AssemblyTypeVersionReference validateAssemblyFileSatic(byte[] data) throws AssemblyLoadException {
		try {
			As2Unit as2Assembly = (As2Unit) JastAddXMLParser.parse(new String(data),
					"se.lth.cs.palcom.assembly.as2.gen.interpreter");
			return as2Assembly.getAs2Assembly().getAssemblyTypeVersionName().getReference();
		} catch (Exception e) {
			throw new AssemblyLoadException(e);
		}
	}

	// @Override
	// public AssemblyContainer loadAssembly(byte[] data, boolean enabled,
	// String instanceName, RuntimeContext runtimeContext)
	// throws AssemblyLoadException {
	// As2AssemblyContainer res = new
	// As2AssemblyContainer(fireEvent,currentDevice, data,instanceName,
	// runtimeContext);
	// if(enabled){
	// res.start();
	// }
	// return res;
	// }

}

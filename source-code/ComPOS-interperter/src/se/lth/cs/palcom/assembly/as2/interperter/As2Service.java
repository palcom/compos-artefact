package se.lth.cs.palcom.assembly.as2.interperter;

import ist.palcom.resource.descriptor.Command;
import ist.palcom.resource.descriptor.DeviceID;
import ist.palcom.resource.descriptor.PRDService;
import ist.palcom.resource.descriptor.Param;
import ist.palcom.resource.descriptor.ServiceID;
import se.lth.cs.palcom.communication.connection.Readable;
import se.lth.cs.palcom.device.AbstractDevice;
import se.lth.cs.palcom.service.AbstractSimpleService;
import se.lth.cs.palcom.service.distribution.UnicastDistribution;

public class As2Service extends AbstractSimpleService {

	public static final ServiceID serviceId = new ServiceID(new DeviceID("X:se.lth.cs.palcom.assembly.as2.As2Service"),
			"1", new DeviceID("X:se.lth.cs.palcom.assembly.as2.As2Service"), "1");

	public As2Service(AbstractDevice container, String comName) {
		super(container, serviceId, "P1", "1", "As2Service", "1" + comName, "A service for the As2Service test", new UnicastDistribution(true));
		Command cmndIn = new Command(comName, null, Command.DIRECTION_IN, "Incoming message to be processed");
		cmndIn.addParam(new Param("inputValue", "text/plain", "input string"));
		getProtocolHandler().addCommand(cmndIn);
	}

	@Override
	protected void invoked(Readable conn, Command command) {
		// TODO Auto-generated method stub

	}
	
	@Override
	public void start() {
		super.start();
		setStatus(PRDService.FULLY_OPERATIONAL, "I'm ready");
	}
	
	@Override
	public void stop() {
		super.stop();
		setStatus(PRDService.NOT_OPERATIONAL, "I'm unready");
	}

}

package se.lth.cs.palcom.assembly.as2.interperter;

import java.util.ArrayList;

import ist.palcom.resource.descriptor.Command;
import ist.palcom.resource.descriptor.PRDService;
import ist.palcom.resource.descriptor.Param;
import ist.palcom.resource.descriptor.ServiceID;
import se.lth.cs.palcom.assembly.as2.gen.interpreter.CommandDef;
import se.lth.cs.palcom.assembly.as2.gen.interpreter.CommandParameter;
import se.lth.cs.palcom.assembly.as2.gen.interpreter.InCommandDef;
import se.lth.cs.palcom.assembly.as2.gen.interpreter.OutCommandDef;
import se.lth.cs.palcom.assembly.as2.gen.interpreter.SynthesizedService;
import se.lth.cs.palcom.assembly.as2.gen.interpreter.WhenDo;
import se.lth.cs.palcom.assembly.as2.interperter.As2Executer.EventToExecute;
import se.lth.cs.palcom.assembly.as2.util.As2ConnInstId;
import se.lth.cs.palcom.command.trace.CommandTracer;
import se.lth.cs.palcom.common.collections.Iterator;
import se.lth.cs.palcom.communication.connection.Connection;
import se.lth.cs.palcom.communication.connection.DirectedConnection;
import se.lth.cs.palcom.communication.connection.Readable;
import se.lth.cs.palcom.communication.connection.SelectorAware;
import se.lth.cs.palcom.communication.connection.Writable;
import se.lth.cs.palcom.device.AbstractDevice;
import se.lth.cs.palcom.service.AbstractSimpleService;
import se.lth.cs.palcom.service.distribution.UnicastDistribution;

public class As2SynthService extends AbstractSimpleService {

	private SynthesizedService pd;
	private CommandTracer ct;
	private As2AssemblyContainer as2container;

	public As2SynthService(AbstractDevice container, ServiceID serviceID, String name,
			String instance, String helpText, SynthesizedService pd, As2AssemblyContainer as2container) {
		super(container, serviceID, "P1", "v1.0", name, instance, helpText, new UnicastDistribution(true));
		this.pd = pd;
		pd.setService(this);
		ct = as2container.getCommandTracer();
		this.as2container = as2container;
	}

	@Override
	protected void invoked(final Readable conn, final Command command) {
		ct.trace(command, "in synth");
		As2StackUtil.setPopTheStackWithSelectorOnTop(command);
		ArrayList<WhenDo> dependentConditions = pd.dependentConditions();
		final DirectedConnection  dc = (DirectedConnection) conn;
		for(final WhenDo sc : dependentConditions){
			if(sc.match(command)){
				As2Executer.addEventToExecute(new EventToExecute() {
					@Override
					public void execute() {
	//					sc.performActions((DirectedConnection) conn,command);
					//	dc.getLocalSelector();
				//		ct.trace(command,"Match synt sel " + dc.getLocalSelector());
						if(command.getSource() == -1) { // I think to get it to work with PUIDI
							command.setSource(12345); 
						}
						if(command.getSeqNbt() == -1) {
							command.setSeqNbr(2);
						}
						as2container.newActivationRecord(new As2ConnInstId(dc.getLocalSelector(), command), sc);
					}
				});
			}
		}
		execute(command, dc);
		
	}
	
	public void execute(final Command command,final DirectedConnection dc) {
		As2Executer.addEventToExecute(new EventToExecute() {
			@Override
			public void execute() {
				Iterator itr = as2container.getActivationRecords().iterator();
				while (itr.hasNext()) {
					ActivationRecord ac = (ActivationRecord) itr.next();
					ac.performUntilWait(dc, command);
				}
			}
		});
	}
	
	public Command findCommand(String s){
		return getProtocolHandler().findCommand(s, Command.DIRECTION_OUT);
	}
	
	public void send(Command com){
	//	System.out.println(com.getReplyStack());
	//	ct.traceSendCommand(com);
		As2StackUtil.pushTheStack(com);
		sendToAll(com);
	}
	
	public void reply(Command com){
		As2StackUtil.pushTheStackWithOne(com);
		//ct.traceSendCommand(com);
		replyTo(com);
		
	}
	
	@Override
	protected void connectionClosed(Connection conn) {
		int sel = -1;
		if(conn instanceof SelectorAware ){
			SelectorAware sCon = (SelectorAware) conn;
			try {
				sel = (sCon.getLocalSelector());
			} catch (IllegalStateException e) {
				
			}
		}
		final int finalSel = sel;
		As2Executer.addEventToExecute(new EventToExecute() {
			
			@Override
			public void execute() {
				as2container.removeActivationForConnection(finalSel);
			}
		});
			
		super.connectionClosed(conn);
	}
	

	
	

	@Override
	public void start() {
		super.start();
		setStatus(PRDService.FULLY_OPERATIONAL, "I'm ready");
	}

	@Override
	public void stop() {
		super.stop();
		setStatus(PRDService.NOT_OPERATIONAL, "Not operational");
	}
	
	public void addInCommands(ArrayList<InCommandDef> inCommands) {
		String dir = Command.DIRECTION_IN;
		addCommands(inCommands, dir);
	}

	void addCommands(ArrayList<? extends CommandDef> commands, String dir) {
		for(CommandDef com : commands){
			String name =com.getNameDef().getName();
			String help = com.hasHelpText() ? com.getHelpText().getText() : "";
			Command command = new Command(name, name,dir, help);
			for(CommandParameter cp : com.getCommandParameters()){
				String namePar = cp.getNameDef().getName();
				String helpPar = cp.hasHelpText() ? cp.getHelpText().getText() : "";
				String typePar = cp.getType().getType();
				Param param = new Param(namePar, namePar, typePar, helpPar);
				command.addParam(param);
			}
			getProtocolHandler().addCommand(command);
		}
	}

	public void addOutCommands(ArrayList<OutCommandDef> outCommands) {
		String dir = Command.DIRECTION_OUT;
		addCommands(outCommands, dir);
	}

	public void deRefAST() {
		pd.setService(null);
		
	}
}
